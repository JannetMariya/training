package com.kpr.training.constants;

public class QueryStatement {
	
	public static final String INSERT_ADDRESS_QUERY = "INSERT INTO `jdbc_demo`.`address`"
															+ "(`street`,`city`,`postal_code`)"
															+ "VALUES (?,?,?)";
	public static final String UPDATE_ADDRESS_QUERY = "UPDATE `jdbc_demo`.`address` "
															+ "SET `street` = (?)"
															+ ", `city` = (?), "
															+ "`postal_code` = (?) "
															+ "WHERE (`id` = (?))";
	public static final String READ_ADDRESS_QUERY = "SELECT `street`"
														+ ", `city` "
														+ ", `postal_code` "
														+ "FROM `jdbc_demo`.`address`"
														+ "WHERE `id` = (?)";
	public static final String READ_ALL_ADDRESS_QUERY = "SELECT `street`"
														   + ",`city`"
														   + ",`postal_code`"
														   + "FROM `jdbc_demo`.`address`" ;
	public static final String DELETE_ADDRESS_QUERY = "DELETE FROM `jdbc_demo`.`address`"
														  + "WHERE (`id` = (?))";
	
	public static final String INSERT_PERSON_QUERY = "INSERT INTO `jdbc_demo`.`person`"
														  +"(`name`,`email`,`address_id`,`birth_date`)" 
														  +"VALUES (?,?,?,?)";
	public static final String READ_PERSON_QUERY = "SELECT `id`"
														+ ",`name`"
														+ ",`email`"
														+ ",`address_id`"
														+ ",`birth_date`"
														+ ",`created_date`"
														+ "FROM `jdbc_demo`.`person`"
														+ "WHERE (`id` = (?))";
	
	public static final String READ_ALL_PERSON_QUERY = "SELECT `id`"
														   	+ ",`name`"
														   	+ ",`email`"
														   	+ ",`address_id`"
														   	+ ",`birth_date`"
														   	+ ",`created_date`"
														   	+ "FROM `jdbc_demo`.`person`";
	
	public static final String DELETE_PERSON = "DELETE FROM `jdbc_demo`.`person`"
															+ "WHERE (`id` = (?))";
	
	public static final String UPDATE_PERSON = "UPDATE `jdbc_demo`.`person` "
															+ "SET `name` = (?)"
															+ ", `email` = (?)"
															+ ", `birth_date` = (?)"
															+ ", `created_date` = (?)"
															+ " WHERE (`id` = (?))";
	
	public static final String UNIQUE_QUERY = "SELECT `id`"
													+ "FROM `jdbc_demo`.`person`"
													+ "WHERE (`email` = (?))";
}
