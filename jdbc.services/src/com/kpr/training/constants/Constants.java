package com.kpr.training.constants;

public class Constants {
	
	public static final String URL = "jdbc:mysql://localhost:3306/jdbc_demo";
	public static final String USER= "root";
	public static final String PASS = "DhushandhanSQL";
	
	public static final String INSERTADDRESSQUERY = "INSERT INTO `jdbc_demo`.`address`"
													+ "(`street`,`city`,`postal_code`)"
													+ "VALUES (?,?,?)";
	public static final String UPDATEADDRESSQUERY = "UPDATE `jdbc_demo`.`address` "
													+ "SET `street` = (?)"
													+ ", `city` = (?), "
													+ "`postal_code` = (?) "
													+ "WHERE (`id` = (?))";
	public static final String READADDRESSQUERY = "SELECT street"
												  + ", `city` "
												  + ", `postal_code` "
												  + "FROM `jdbc_demo`.`address`"
												  + "WHERE `id` = (?)";
	public static final String READALLADDRESSQUERY = "SELECT `street`"
			   										 + ",`city`"
			   										 + ",`postal_code`"
			   										 + "FROM `jdbc_demo`.`address`" ;
	public static final String DELETEADDRESSQUERY = "DELETE FROM `jdbc_demo`.`address`"
			  										+ "WHERE (`id` = (?))";
	
	public static final String INSERTPERSONQUERY = "INSERT INTO person(?,?,?,?)";
	public static final String READPERSONQUERY = "SELECT id"
												 + ",name"
												 + ",email"
												 + ",address_id"
												 + ",birth_date"
												 + ",created_date"
												 + "FROM jdbc_demo.person"
												 + "WHERE (`id` = (?))";
	
	public static final String READALLPERSONQUERY = "SELECT id"
												   	+ ",name"
												   	+ ",email"
												   	+ ",address_id"
												   	+ ",birth_date"
												   	+ ",created_date"
												   	+ "FROM jdbc_demo.person";
	
	public static final String DELETEPERSON = "DELETE FROM jdbc_demo.person"
											  + "WHERE (`id` = (?))";
	
	public static final String UPDATEPERSON = "UPDATE person SET `name` = (?)"
											  + ", `email` = (?)"
											  + ", `birth_date` = (?)"
											  + ", `created_date` = (?)"
											  + " WHERE (`id` = (?))";
	public static String pass;

}
