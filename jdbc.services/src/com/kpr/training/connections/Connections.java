package com.kpr.training.connections;

/**
 * Problem Statement
 * 1.Create a Connection Class
 * 
 * Entity
 * 1.Connections
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a Connection object called connection
 * 2.Pass the Constents url, Constents user, Constents pass to the getConnection method..
 * 3.Catch the SQL Exception
 * 4.return the connection.
 * 
 * Pseudo Code
 * 
 * class Connections {
 *	
 *	public static Connection connection ;
 *	
 *	public static Connection getConnection() {
 * 		try {
 *			connection = DriverManager.getConnection(Constents.url, Constents.user, Constents.pass);
 *		} catch (SQLException e) {
 *			e.printStackTrace();
 *		}
 *		return connection;
 *	}
 *}
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.kpr.training.constants.Constants;
import com.kpr.training.exception.AppException;

public class Connections {
	
	public static Connection connection ;
	private static Properties property;
	
	public static Connection getConnection() throws AppException{
		
		setProperty(new Properties());
		
		try {
			/*connection = DriverManager
						.getConnection(property.getProperty("sqlUrl")
						,property.getProperty("sqlUser") 
						,property.getProperty("sqlPass"));
			*/
			
			connection = DriverManager.getConnection(Constants.URL,Constants.USER,Constants.pass);
			if(connection != null) {
				System.out.println("Connection Established Successfully");
			} else {
				throw new AppException("401");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public static Properties getProperty() {
		return property;
	}

	public static void setProperty(Properties property) {
		Connections.property = property;
	}
}
