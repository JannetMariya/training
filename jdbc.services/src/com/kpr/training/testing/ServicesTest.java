package com.kpr.training.testing;

import java.sql.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.service.ConnectionService;
import com.kpr.training.service.PersonService;
import com.mysql.jdbc.Connection;

/**
 * Problem Statement
 * 1.To Test All possible conditions for Service Classes
 * 
 * Requirement
 * 1.To Test All possible conditions for Service Classes
 * 
 * Entity
 * 1.ServicesTest
 * 2.PersonService
 * 3.AppException
 * 4.Connection
 * 5.Person
 * 6.Address
 * 
 * Method Signature
 * 1.public void connectionTest();
 * 2.public void insertPersonWithAddress();
 * 3.public void insertPersonDuplicate();
 * 4.public void insertPersonNoAddress();
 * 5.public void insertPersonNoFields();
 * 6.public void readPersonWithAddress();
 * 7.public void readPersonWithoutAddress();
 * 
 * Jobs to be Done
 * 1.Create a method called connectionTest
 * 2.Create a connection object called Connection type and 
 * 		call the getConnectio method of Connections Class and store it.
 * 3.Compare the result of the connection with Assert and assertEquals
 * 4.Create a method called insertPersonWithAddress 
 * 5.Create Date of Date type
 * 6.Create Person of person type and pass values
 * 7.Create Address of address type and pass values
 * 8.Create a personService object PersonService
 * 9.Call the insert method of personService class and pass the values,
 *     store the result in insertPersonStatus object of long type.
 * 10.Check whether the insertPersonStatus object is greater than 0 with AssertTrue of Assert.
 * 11.Create a method called insertPersonDuplicate 
 * 12.Create date of Date type.
 * 13.Create person of Person type
 * 14.Create address of Address type
 * 15.Create PersonService object called personService
 * 16.Call the insert method from the personService class and pass the person and address
 * 		and store to insertPersonStatus 
 * 17.Check the insertPersonStatus is greater than 0 with assertTrue of Assert
 * 18.Create method called insertPersonNoAddress
 * 19.Create a Date and person and pass the values for person
 * 20.Create personService object of PersonService class
 * 21.Check the insertPersonStatus is greater than 0 with assertTrue
 * 22.Create method called insertPersonNoFields.
 * 23.Create personService object of PersonService
 * 24.Create person and address type of null values
 * 25.call the insert method of personService class and pass the person and address
 * 25.Store the insert method values to the insertPersonService
 * 26.Check the insertPersonStatus is equal to 0 with assertTrue of Assert
 * 27.Create method called readPersonWithAddress 
 * 28.Create person and address with null values
 * 29.Create personService object of PersonService 
 * 30.Call the readPerson method of personService and pass the values to the parameter and 
 * 		store to the objectList of Object List type
 * 31.Check the objectList is not equal to null with assertTrue of Assert
 * 32.Create a readPersonWithoutAddress method
 * 33.Create Person and Address objects called person and address with no values
 * 34.Create a personService object of PersonService
 * 35.Call the readPerson method of peronService and pass the values to the method and store 
 * 		it to listObject of List<Oject>
 * 36.Check the listObject is not equal to null with assertTrue in Assert.
 * 
 * Pseudo Code
 * 
 * class ServicesTest {
 *	
 *	long insertPersonStatus = 0; 
 *	
 *	@Test(priority = 1, description = "Connection Establishment") 
 *	public void connectionTest() throws AppException {
 *		Connection connection = (Connection) Connections.getConnection();
 *		
 *		Assert.assertEquals(connection, connection);
 *	}
 *	
 *	@Test(priority = 2, description = "Person Insertion with Address")
 *	public void insertPersonWithAddress() throws AppException {
 *		Date date = new Date(1987,05,11);
 *		Person person = new Person("Kowsika","kowsika@gmail.com",date);
 *		Address address = new Address("Tirupur","Tirupur",87908);
 *		PersonService personService = new PersonService();
 *		insertPersonStatus = personService.insert(person, address);
 *		
 *		Assert.assertTrue(insertPersonStatus > 0);
 *	}
 *	
 *	@Test(priority = 3, description = "Person Insertion Duplicate")
 *	public void insertPersonDuplicate() throws AppException {
 *		Date date = new Date(1987,05,11);
 *		Person person = new Person("Kowsika","kowsika@gmail.com",date);
 *		Address address = new Address("Tirupur","Tirupur",125331);
 *		PersonService personService = new PersonService();
 *		insertPersonStatus = personService.insert(person, address);
 *		
 *		Assert.assertTrue(insertPersonStatus > 0);
 *	}
 *	
 *	@Test(priority = 4, description = "Person Insertion without Address")
 *	public void insertPersonNoAddress() throws AppException{
 *		Date date = new Date(1987,12,12);
 *		Person person = new Person("Aadhi vaasi","aadhivaasi@gmail.com",date);
 *		
 *		PersonService personService = new PersonService();
 *		insertPersonStatus = personService.insert(person,null);
 *		
 *		Assert.assertTrue(insertPersonStatus > 0);
 *	}
 *	
 *	@Test(priority = 5, description = "Person Insertion without any fields")
 *	public void insertPersonNoFields() throws AppException {
 *		PersonService personService = new PersonService();
 *		Person person = new Person(null, null, null);
 *		Address address = new Address(null, null, 0);
 *		insertPersonStatus = personService.insert(person, address);
 *		
 *		Assert.assertTrue(insertPersonStatus == 0);
 *	}
 *	
 *	@Test(priority = 6, description = "Read Person with Address")
 *	public void readPersonWithAddress() throws AppException {
 *		
 *		Person person = new Person(null, null, null);
 *		Address address = new Address(null, null, insertPersonStatus);
 *		
 *		PersonService personService = new PersonService();
 *		List<Object> objectList = personService.readPerson(30, true);
 *		
 *		Assert.assertTrue(objectList != null);
 *	}
 *	
 *	@Test(priority = 7, description = "Read Person without Address")
 *	public void readPersonWithoutAddress() throws AppException {
 *		Person person = new Person(null, null, null);
 *		Address address = new Address(null, null, insertPersonStatus);
 *		
 *		PersonService personService = new PersonService();
 *		
 *		List<Object> listObject = personService.readPerson(32, false);
 *		
 *		Assert.assertTrue(listObject != null);
 *	}
 *}
 * @author UKSD
 *
 */

public class ServicesTest {
	
	long insertPersonStatus = 0; 
	
	@Test(priority = 1, description = "Connection Establishment")
	public void connectionTest() throws Exception {
		Connection connection = (Connection) ConnectionService.initConnection();
		
		Assert.assertTrue(connection != null);
		
		ConnectionService.releaseConnection(connection);
	}
	
	@Test(priority = 2, description = "Person Insertion with Address")
	public void insertPersonWithAddress() throws Exception {
		Date date = new Date(2001,98,18);
		Person person1 = new Person("Dong Lee","dontlee7amArivu@gmail.com",date);
		Address address1 = new Address("Ching mung Street","Ching Ching",118624);
		PersonService personService = new PersonService();
		insertPersonStatus = personService.create(person1, address1);
		
		Assert.assertTrue(insertPersonStatus > 0);
	}
	
	@Test(priority = 3, description = "Person Insertion Duplicate")
	public void insertPersonDuplicate() throws Exception {
		Date date = new Date(1987,05,11);
		Person person = new Person("Kowsika","kowsika@gmail.com",date);
		Address address = new Address("Tirupur","Tirupur",125331);
		PersonService personService = new PersonService();
		insertPersonStatus = personService.create(person, address);
		
		Assert.assertTrue(insertPersonStatus > 0);
	}
	
	@Test(priority = 4, description = "Person Insertion without Address")
	public void insertPersonNoAddress() throws Exception{
		Date date = new Date(1987,12,12);
		Person person = new Person("Aadhi vaasi","aadhivaasi@gmail.com",date);
		
		PersonService personService = new PersonService();
		insertPersonStatus = personService.create(person,null);
		
		Assert.assertTrue(insertPersonStatus > 0);
	}
	
	@Test(priority = 5, description = "Person Insertion without any fields")
	public void insertPersonNoFields() throws Exception {
		PersonService personService = new PersonService();
		Person person = new Person(null, null, null);
		Address address = new Address(null, null, 0);
		insertPersonStatus = personService.create(person, address);
		
		Assert.assertTrue(insertPersonStatus == 0);
	}
	
	@Test(priority = 6, description = "Read Person with Address")
	public void readPersonWithAddress() throws Exception {
		
		Date date = new Date(2001,98,18);
		Person person1 = new Person("Dong Lee","dontlee7amArivu@gmail.com",date);
		Address address1 = new Address("Ching mung Street","Ching Ching",118624);
		
		PersonService personService = new PersonService();
		List<Object> objectList = personService.readPerson(person1, true);
		
		Assert.assertTrue(objectList != null);
	}
	
	@Test(priority = 7, description = "Read Person without Address")
	public void readPersonWithoutAddress() throws Exception {
		
		Date date = new Date(2001,98,18);
		Person person1 = new Person("Dong Lee","dontlee7amArivu@gmail.com",date);
		Address address1 = new Address("Ching mung Street","Ching Ching",118624);
		
		PersonService personService = new PersonService();
		
		List<Object> listObject = personService.readPerson(person1, false);
		
		Assert.assertTrue(listObject != null);
	}
}
