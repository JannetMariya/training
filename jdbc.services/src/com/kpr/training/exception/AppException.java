package com.kpr.training.exception;

/**
 * Problem Statement
 * 1.Create a AppException
 * 
 * Entity
 * 1.AppException
 * 
 * Method Signature
 * 1.public AppException();
 * 2.public AppException(ErrorCodes code);
 * 3.public int getErrorCode();
 * 4.public String getErrorMessage();
 * 
 * Jobs to be Done
 * 1.Create a variable called code of int type
 * 2.Create a variable called message of type String
 * 3.Create a empty paramater constructor 
 * 4.Create a constructor with ErrorCodes type code as parameter
 * 		4.1)store the code.getCode() to this.code
 * 		4.2)Stroe the code.getMessage() to this.message
 * 5.Create a method of int return type called getErrorCode and return code 
 * 6.Create a method called getErrorMessage that return String and return message
 * 
 * Pseudo Code
 * 
 * class AppException extends Exception{
 *	
 *	public int code;
 *	public String message;
 *	
 *	public AppException() {
 *		super();
 *	}
 *	
 *	public AppException(ErrorCodes code) {
 *		this.code = code.getCode();
 *		this.message = code.getMessage();
 *	}
 *	
 *	public int getErrorCode() {
 *		return code;
 *	}
 *	
 *	public String getErrorMessage() {
 *		return message;
 *	}
 *	
 *}
 */		

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class AppException extends Exception{
	
	public int code;
	public String message;
	
	public AppException() {
		super();
	}
	
	public AppException(String string) {
		this.code = string.getCode();
		this.message = string.getMessage();
	}
	
	public int getErrorCode() {
		return code;
	}
	
	public String getErrorMessage() {
		return message;
	}
	
}
