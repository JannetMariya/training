package com.kpr.training.exception;

/**
 * Problem Statement
 * 1.Create a ErrorCodes Class with Codes and messages
 * 
 * Entity
 * 1.ErrorCode
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a String field as conSuccess and initialize Connection Successful
 * 2.Create a String field as postalError and initialize Postal Code should no be Empty
 * 3.Create a String field as addressError and initialize Address Creation Failure
 * 4.Create a String field as fieldEmpty and initialize The Id Field cannot be Empty
 * 5.Create a String field as updateFailure and initialize Updation Failure
 * 6.Create a String field as fieldsError and initialize Fields can be Empty
 * 7.Create a String field as personCreation and initialize Person Creation Failure
 * 8.Create a String field as personRead and initialize Person Read Failure
 * 9.Create a String field as personId and initialize Person id cannot be Empty
 * 10.Create a String field as personDelete and initialize Person Delete Failure
 * 11.Create a String field as personUpdate and initialize Person Updation Failure
 * 12.Create a String field as addressnotFound and initialize Address Not Found
 * 13.public static String uniqueEmail = "Email ID is not Unique";
 * 
 * Pseudo Code
 * 
 * class ErrorCode {
 *	
 *	public static String conFailure = "Connection Failure";
 *	public static String postalError = "Postal Code should not be Empty";
 *	public static String addressError = "Address Creation Failure";
 *	public static String fieldEmpty = "The Id Field cannot be Empty";
 *	public static String updateFailure = "Updation Failure";
 *	public static String fieldsError = "Fields can be Empty";
 *	public static String personCreation = "Person Creation Failure";
 *	public static String personRead = "Person Read Failure";
 *	public static String personId = "Person id cannot be Empty";
 *	public static String personDelete = "Person Delete Failure";
 *	public static String personUpdate = "Person Updation Failure";
 *	public static String addressNotFound = "Address Not Found";
 *  public static String uniqueEmail = "Email ID is not Unique";
 *	
 *} 
 * @author UKSD
 *
 */

public class ErrorCode {
	
	public static String conFailure = "Connection Failure";
	public static String postalError = "Postal Code should not be Empty";
	public static String addressError = "Address Creation Failure";
	public static String fieldEmpty = "The Id Field cannot be Empty";
	public static String updateFailure = "Updation Failure";
	public static String fieldsError = "Fields can be Empty";
	public static String personCreation = "Person Creation Failure";
	public static String personRead = "Person Does not Exist";
	public static String personId = "Person id cannot be Empty";
	public static String personDelete = "Person Delete Failure";
	public static String personUpdate = "Person Updation Failure";
	public static String addressNotFound = "Address Not Found";
	public static String uniqueEmail = "Email ID is not Unique";
	public static String personExist = "Person already Exists";
	public static String propError = "Properties Fetch Error";
	public static String conCloseFailure = "Facing Error while Closing Connection";
	
}

