package com.kpr.training.exception;

/**
 * Problem Statement
 * 1.Create ErrorCode with Enum
 * 
 * Entity
 * 1.ErrorCodes
 * 
 * Method Signature
 * 1.ErrorCodes(int code, String message);
 * 2.public int getCode();
 * 3.public String getMessage();
 * 
 * Jobs to be Done
 * 1.Create variable called code of type int
 * 2.Create a variable called message of type String
 * 3.Create a Constructor with code and message as parameter
 * 4.Create another method called getCode of return type int that returns code
 * 5.Create another method called getMessage of return type String that returns message
 * 6.Create Enum type ErrorCodes with Code and Message
 * 
 * Pseudo Code
 * 
 * enum ErrorCodes {
 *	
 *	CON_FAILURE(401, "Connection Failure"),
 *	POSTAL_ERROR(402, "Postal Code should not be Empty"),
 *	ADDRESS_ERROR(403, "Address Creation Failure"),
 *	FIELD_EMPTY(404, "The Id Field cannot be Empty"),
 *	UPDATE_FAILURE(405, "Updation Failure"),
 *	FIELDS_ERROR(406, "Person Creation Failure"),
 *	PERSON_CREATION(407, "Person Does not Exist"),
 *	PERSON_READ(408, "Person id cannot be Empty"),
 *	PERSON_ID(409, "Person Delete Failure"),
 *	PERSON_DELETE(410, "Person Updation Failure"),
 *	PERSON_UPDATE(411, "Address Not Found"),
 *	ADDRESS_NOT_FOUND(412, "Email ID is not Unique"),
 *	UNIQUE_EMAIL(413, "Person already Exists"),
 *	PERSON_EXIST(414, "Person already Exists"),
 *	PROP_ERROR(415, "Properties Fetch Error"),
 *	CON_CLOSE_FAILURE(416,"Facing Error while Closing Connection");
 *	
 *	public int code;
 *	public String message;
 *	
 *	ErrorCodes(int code, String message) {
 *		this.code = code;
 *		this.message = message;
 *	} 
 *
 *	public int getCode() {
 *		return code;
 *	}
 *	
 *	public String getMessage() {
 *		return message;
 *	} 
 *
 *}
 * 
 * 
 * 
 * @author UKSD
 *
 */

public enum ErrorCodes {
	
	CON_FAILURE(401, "Connection Failure"),
	POSTAL_ERROR(402, "Postal Code should not be Empty"),
	ADDRESS_ERROR(403, "Address Creation Failure"),
	FIELD_EMPTY(404, "The Id Field cannot be Empty"),
	UPDATE_FAILURE(405, "Updation Failure"),
	FIELDS_ERROR(406, "Person Creation Failure"),
	PERSON_CREATION(407, "Person Does not Exist"),
	PERSON_READ(408, "Person id cannot be Empty"),
	PERSON_ID(409, "Person Delete Failure"),
	PERSON_DELETE(410, "Person Updation Failure"),
	PERSON_UPDATE(411, "Address Not Found"),
	ADDRESS_NOT_FOUND(412, "Email ID is not Unique"),
	UNIQUE_EMAIL(413, "Person already Exists"),
	PERSON_EXIST(414, "Person already Exists"),
	PROP_ERROR(415, "Properties Fetch Error"),
	CON_CLOSE_FAILURE(416,"Facing Error while Closing Connection");
	
	public int code;
	public String message;
	
	ErrorCodes(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}

}
