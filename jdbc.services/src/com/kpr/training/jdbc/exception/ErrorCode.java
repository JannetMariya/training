package com.kpr.training.jdbc.exception;

/**
 * Problem Statement
 * 1.Create a ErrorCodes Class with Codes and messages
 * 
 * Entity
 * 1.ErrorCode
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a String field as conSuccess and initialize Connection Successful
 * 
 *
 */

public class ErrorCode {
	
	public static final char[] personId = null;
	public static final char[] updateFailure = null;
	public static String conFailure = "Connection Failure";
	public static String postalError = "Postal Code should not be Empty";
	public static String addressError = "Address Creation Failure";
	public static String fieldEmpty = "The Id Field cannot be Empty";
	public static char[] fieldsError;
	public static char[] personCreation;
	public static char[] personRead;
	public static char[] personDelete;
	public static char[] personUpdate;
	
}
