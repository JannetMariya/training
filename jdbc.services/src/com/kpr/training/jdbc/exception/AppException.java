package com.kpr.training.jdbc.exception;

@SuppressWarnings("serial")
public class AppException extends Exception{
	
	public AppException() {
		super();
	}
	
	public AppException(String code) {
		
		//ErrorCode errCode = new ErrorCode();
		
		if (code.equals("401" )) {
			System.out.println(ErrorCode.conFailure);
		} if (code.equals("402" )) {
			System.out.println(ErrorCode.postalError);
		} if (code.equals("403")) {
			System.out.println(ErrorCode.addressError);
		} if (code.equals("404")) {
			System.out.println(ErrorCode.fieldEmpty);
		} if (code.equals("405" )) {
			System.out.println(ErrorCode.updateFailure);
		} if (code.equals("406" )) {
			System.out.println(ErrorCode.fieldsError);
		} if(code.equals("407")) {
			System.out.println(ErrorCode.personCreation);
		} if(code.equals("408")) {
			System.out.println(ErrorCode.personRead);
		} if(code.equals("409")) {
			System.out.println(ErrorCode.personId);
		} if(code.equals("410")) {
			System.out.println(ErrorCode.personDelete);
		} if(code.equals("411")) {
			System.out.println(ErrorCode.personUpdate);
		}	
	}
	
}
