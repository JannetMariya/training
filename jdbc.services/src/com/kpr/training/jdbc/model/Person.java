package com.kpr.training.jdbc.model;

import java.sql.Date;

public class Person {
	
	public long id;
	public String name;
	public String email;
	public long address_id;
	public Date birth_date;
	public Date created_date;
	
	public Person(long id, String name, String email, long address_id, Date birth_date, Date created_date) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.address_id = address_id;
		this.birth_date = birth_date;
		this.created_date = created_date;
	}
	
	public long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public long getAddressId() {
		return address_id;
	}
	
	public Date getBirthDate() {
		return birth_date;
	}
	
	public Date getCreatedDate() {
		return created_date;
	}

}
