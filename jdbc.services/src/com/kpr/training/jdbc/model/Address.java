package com.kpr.training.jdbc.model;

public class Address {

	public static long id;
	public String street;
	public String city;
	public long postal_code;
	
	public Address(String street, String city, long postal_code) {
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public long getPostalCode() {
		return postal_code;
	}
	
}
