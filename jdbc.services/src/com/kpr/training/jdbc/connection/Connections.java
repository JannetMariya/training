package com.kpr.training.jdbc.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connections {
	
	public static final String url = "jdbc:mysql://root@127.0.0.1:3306";
	public static final String user = "root";
	public static final String pass = "yoga@2001";
	
	public static Connection connection ;
	
	public static Connection getConnection() {
		try {
			connection = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
		

	}
}
