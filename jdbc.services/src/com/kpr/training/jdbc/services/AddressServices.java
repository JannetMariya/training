/*
 Problem Statement
    1.Perform CRUD Operations for Address Service
  
1.Requirement
   - Perform CRUD Operations for Address Service
  
2.Entity
    1.Address
    2.AddressService
    3.AppException
    4.ErrorCode
    
3.Method Declaration
    1.public static long insertAddress(Address address)
    2.public static Address readAddress(long id) 
    3.public List<Address> readAllAddress()
    4.public int updateAddress(Address address, long id)
    5.public Boolean deleteAddress(long id)
  
3.Jobs to be Done
    1.Create a Address uisng invoke InsertAddress class insertAddress method.
    2.Read an Address using invoke ReadAddress class readAddress method.
    3.Read all Addresses.
    4.Update an Address using invoke UpdateAddress class updateAddress method.
    5.Delete an Address.
    
*/
package com.kpr.training.jdbc.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.kpr.training.jdbc.connection.Connections;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.model.Address;

public class AddressServices {

	static long addressId;
	public static int count;
	public static String street;
	public static String city;
	public static int postal_code;
	public static Address addressGot;
	

	public static long insertAddress(Address address) throws SQLException {
		try {
			if (address.getPostalCode() == 0) {
				throw new AppException("402");
			}

			Connection connection = Connections.getConnection();

			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertAddressQuery,
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());

			addressId = statement.executeUpdate();

			if (addressId == 0) {
				throw new AppException("403");
			}

		} catch (AppException e) {
			e.printStackTrace();
		}

		return addressId;
	}

	public static void readAddress(long id) throws SQLException {
			Connection con = Connections.getConnection();  
			Statement statement = con.createStatement();
			ResultSet result = statement.executeQuery(QueryStatement.readAddressQuery);
			 
			if (result.next() ){
			    String street = result.getString("street");
			    String city = result.getString("city");
			    String postal_code = result.getString("postal_code");
			    System.out.println( city+" "+street+" "+postal_code);
			}
	}

	public static void readAllAddress() throws SQLException {
		Connection con = Connections.getConnection();  
		Statement statement = con.createStatement();
		ResultSet result = statement.executeQuery(QueryStatement.readAllAddressQuery);
		 
		while (result.next()){
		    String street = result.getString("street");
		    String city = result.getString("city");
		    String postal_code = result.getString("postal_code");
		    System.out.println( city+" "+street+" "+postal_code);
		}
	}

	public static int updateAddress(Address address, long id) throws SQLException {
		try {

			if (address.getPostalCode() == 0) {
				throw new AppException("402");
			}
			Connection connection = Connections.getConnection();

			PreparedStatement statement = connection.prepareStatement(QueryStatement.updateAddressQuery);

			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());
			statement.setLong(4, id);

			count = statement.executeUpdate();

			if (count == 0) {
				throw new AppException("403");
			}
		} catch (AppException e) {
			e.printStackTrace();
		}

		return count;
	}

	public static Boolean deleteAddress(long id) throws SQLException {
		Connection con = Connections.getConnection();
		PreparedStatement statement = con.prepareStatement(QueryStatement.deleteAddressQuery);
		statement.setLong(1, id);
		int rowsDeleted = statement.executeUpdate();
		if (rowsDeleted == 0) {
			return true;
		}
		return false;
	}
}
