package com.kpr.training.jdbc.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.jdbc.connection.Connections;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.model.Person;

/**
 * Problem Statement
 * 1.Perform CRUD Operations for Person Service
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public long insert(Person person);
 * 2.public Person readPerson(long id);
 * 3.public List<Person> readAllPerson();
 * 4.public long update(Person person);
 * 5.public long delete(long id);
 * 
 * Jobs to be Done
 * 1.Create a Address
 * 2.Read an Address
 * 3.Read all Addresses
 * 4.Update an Address
 * 5.Delete an Address
 * @author UKSD
 *
 */

public class PersonService {
	
	//Insert Person
	
	private Person person;

	public long insert(Person person) throws AppException {
			
		if(extracted(person).getName() == null && extracted(person).getEmail() == null && extracted(person).getBirthDate() == null ) {
			throw new AppException("407");
		}
			
		Connection connection = Connections.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updatePerson);
				
			statement.setString(1, extracted(person).getName());
			statement.setString(2, extracted(person).getEmail());
			statement.setLong(3, extracted(person).getAddressId());
			statement.setDate(4, extracted(person).getBirthDate());
				
			extracted(person).id = statement.executeUpdate();
				
			if(extracted(person).id == 0) {
				throw new AppException("407");
			}
				
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return extracted(person).id;
	}
	
	//Read Person
	
	public Person readPerson(long id) throws AppException {
		
		person = null;
		if(id == 0) {
			throw new AppException("409");
		}
		
		Connection connection = Connections.getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllPersonQuery);
			
			statement.setLong(1, id);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				person.id = result.getLong("id");
				person.name = result.getString("name");
				person.email = result.getString("email");
				person.address_id = result.getLong("address_id");
				person.birth_date = result.getDate("birth_date");
				person.created_date = result.getDate("created_date");
			}
			
			if(person == null) {
				throw new AppException("409");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return person;
	}
	
	//ReadAll Person
	
	@SuppressWarnings("unused")
	public List<Person> readAllPerson() throws AppException {
		
		List<Person> allPerson = new ArrayList<>();
		
		Person person = null;
		
		Connection connection = Connections.getConnection();
		
		if(connection == null) {
			throw new AppException("401");
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllPersonQuery);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				extracted(person).id = result.getLong("id");
				extracted(person).name = result.getString("name");
				extracted(person).email = result.getString("email");
				extracted(person).address_id = result.getLong("address_id");
				extracted(person).birth_date = result.getDate("birth_date");
				extracted(person).created_date = result.getDate("created_date");
				
				allPerson.add(extracted(person));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(allPerson == null) {
			throw new AppException("408");
		}
		return allPerson;
	}

	private Person extracted(Person person) {
		return person;
	}
	
	//Update Person
	
	public long update(Person person) throws AppException {
		
		int changes = 0;
			
		if(extracted(person).getId() == 0) {
			throw new AppException("409");
		}
			
		Connection connection = Connections.getConnection();
			
		if(connection == null) {
			throw new AppException("401");
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updatePerson);
				
			statement.setString(1,extracted(person).getName());
			statement.setString(1,extracted(person).getEmail());
			statement.setDate(1,extracted(person).getBirthDate());
			statement.setDate(1,extracted(person).getCreatedDate());
			statement.setLong(1,extracted(person).getId());
				
			changes = statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		if(changes == 0) {
			throw new AppException("411");
		}
			
		return changes;
	}
	
	//Delete Person
	
	public long delete(long id) throws AppException {
		
		long changes = 0;
			
		if(id == 0) {
			throw new AppException("409");
		}
			
		Connection connection = Connections.getConnection();
			
		if(connection == null) {
			throw new AppException("401");
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.deletePerson);
				
			changes = statement.executeUpdate(); 
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		if(changes == 0) {
			throw new AppException("410");
		}
		return changes;
	}
	
}
