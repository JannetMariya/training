package com.kpr.training.jdbc.constant;



public class QueryStatement {
	
	public static final String insertAddressQuery = "INSERT INTO `jdbc`.`address`"
															+ "(`street`,`city`,`postal_code`)"
															+ "VALUES (?,?,?)";
	public static final String updateAddressQuery = "UPDATE `jdbc`.`address` "
															+ "SET `street` = (?)"
															+ ", `city` = (?), "
															+ "`postal_code` = (?) "
															+ "WHERE (`id` = (?))";
	public static final String readAddressQuery = "SELECT street"
														+ ", `city` "
														+ ", `postal_code` "
														+ "FROM `jdbc`.`address`"
														+ "WHERE `id` = (?)";
	public static final String readAllAddressQuery = "SELECT `street`"
														   + ",`city`"
														   + ",`postal_code`"
														   + "FROM `jdbc`.`address`" ;
	public static final String deleteAddressQuery = "DELETE FROM `jdbc`.`address`"
														  + "WHERE (`id` = (?))";
	
	public static final String insertPersonQuery= "INSERT INTO `jdbc`.`person`"
														  +"(`name`,`email`,`address_id`,`birth_date`)" 
														  +"VALUES (?,?,?,?)";
	public static final String readPersonQuery = "SELECT `id`"
														+ ",`name`"
														+ ",`email`"
														+ ",`address_id`"
														+ ",`birth_date`"
														+ ",`created_date`"
														+ "FROM `jdbc`.`person`"
														+ "WHERE (`id` = (?))";
	
	public static final String readAllPersonQuery = "SELECT `id`"
														   	+ ",`name`"
														   	+ ",`email`"
														   	+ ",`address_id`"
														   	+ ",`birth_date`"
														   	+ ",`created_date`"
														   	+ "FROM `jdbc`.`person`";
	
	public static final String deletePerson = "DELETE FROM `jdbc`.`person`"
															+ "WHERE (`id` = (?))";
	
	public static final String updatePerson = "UPDATE `jdbc`.`person` "
															+ "SET `name` = (?)"
															+ ", `email` = (?)"
															+ ", `birth_date` = (?)"
															+ ", `created_date` = (?)"
															+ " WHERE (`id` = (?))";
}
