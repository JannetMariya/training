package com.kpr.training.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.testng.annotations.Test;

import com.kpr.training.constants.Constants;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;

/**
 * Problem Statement
 * 1.Create a Connection Class
 * 
 * Entity
 * 1.ConnectionService
 * 
 * Method Signature
 * 1.public static Properties loadProperties();
 * 2.public static Connection initsConnection();
 * 3.public static void releaseConnection(Connection connection);
 * 
 * Jobs to be Done
 * 1.Create a Connection object called connection and initialize as null
 * 2.Create a method called loadProperties that returns Properties
 * 3.Create a Properties object called property 
 * 4.Create a InputStream object called inputStream and pass the db.properties file path 
 * 	 as parameter
 * 5.Load the property from the inputStream to property object
 * 6.Close the inputStream
 * 7.return the property
 * 8.Create another method called intiConnection that returns Connection 
 * 9.Create a Properties object called dbproperty and call the loadProperties method 
 * 10.Store the Properties from the file to new String
 * 11.SetAutoCommit to false for connection
 * 12.Create a Connection with the getConnection method of DriverManager by passing the 
 *    URL,USER,PASS
 * 13.Check whether the Connection is not equal to null
 * 		13.1)Set Commit to Connection 
 * 		13.2)print Connection Established Successfully
 * 14.Else throw new AppException("401");
 * 15.return the connection;
 * 16.Create a new method called releaseConnection that has a void return type and throws Exception
 * 17.try close the connection
 * 18.Else throw new AppException("416") 
 * 
 * Pseudo Code
 * 
 * class ConnectionService {
 *	
 *	public static Connection connection = null;
 *	
 *	public static Properties loadProperties() throws Exception{
 *		
 *		Properties property = new Properties();
 *		
 *		InputStream inputStream = 
 *				new FileInputStream("C:\\1Dev\\Team\\java\\jdbc-services\\resource\\db.properties");
 *		
 *	 	property.load(inputStream);
 *		
 *		inputStream.close();
 *		
 *		return property;
 *	}
 *	
 *	
 *	public static Connection initConnection() throws Exception{
 *		
 *		Properties dbProperty = ConnectionService.loadProperties();
 *		
 *		if(dbProperty == null) {
 *			throw new AppException("415");
 *		}
 *		
 *		String JDBC_URL = dbProperty.getProperty("URL");
 *		String JDBC_USER = dbProperty.getProperty("USER");
 *		String JDBC_PASS = dbProperty.getProperty("PASS");
 *		
 *		connection.setAutoCommit(false);
 *		
 *		connection = DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASS);
 *		if(connection != null) {
 *			connection.commit();
 *			System.out.println("Connection Established Successfully");
 *		} else {
 *			throw new AppException("401");
 *		}
 *		return connection;
 *	}
 *	
 * 	public static void releaseConnection(Connection connection) throws Exception {
 *		try {
 *			connection.close();
 *		} catch(Exception e) {
 *			throw new AppException("416");
 *		}
 *	}
 *	
 *}
 * 
 */

public class ConnectionService {
	
	public static Connection connection;
	
	public static Properties loadProperties() throws Exception{
		
		Properties property = new Properties();
		
		InputStream inputStream = 
				new FileInputStream("C:\\1Dev\\Team\\java\\jdbc-services\\resource\\db.properties");
		
		property.load(inputStream);
		
		inputStream.close();
		
		return property;
	}
	
	
	public static Connection initConnection() throws Exception{
		
		Properties dbProperty = ConnectionService.loadProperties();
		
		if(dbProperty == null) {
			throw new AppException(ErrorCodes.PROP_ERROR);
		}
		
		String JDBC_URL = dbProperty.getProperty("URL");
		String JDBC_USER = dbProperty.getProperty("USER");
		String JDBC_PASS = dbProperty.getProperty("PASS");
		
		connection = DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASS);
		connection.setAutoCommit(false);
		if(connection != null) {
			connection.commit();
			System.out.println("Connection Established Successfully");
		} else {
			throw new AppException(ErrorCodes.CON_FAILURE);
		}
		return connection;
	}
	
	public static void releaseConnection(Connection connection) throws Exception {
		try {
			connection.close();
		} catch(Exception e) {
			throw new AppException(ErrorCodes.CON_CLOSE_FAILURE);
		}
	}
	
	public static void main(String[] args) {
		try {
			Connection connection = ConnectionService.initConnection();
			
			
			ConnectionService.releaseConnection(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
