package com.kpr.training.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.kpr.training.service.ConnectionService;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.mysql.jdbc.Statement;

/**
 * Problem Statement
 * 1.Perform CRUD Operations for Person Service
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public long insert(Person person);
 * 2.public Person readPerson(long id);
 * 3.public List<Person> readAllPerson();
 * 4.public long update(Person person);
 * 5.public long delete(long id);
 * 
 * Jobs to be Done
 * 1.Create a Address
 * 2.Read an Address
 * 3.Read all Addresses
 * 4.Update an Address
 * 5.Delete an Address
 * @author UKSD
 *
 */

public class PersonService {
	
	//Insert Person
	
	/*
	public int insert(Person person, Address address) throws AppException {
		 
		 long address_id = 0;
		 AddressService addressObject = null;
		 long deleteStatus = 0;
		 int personResult = 0;
		 Person readPerson = null;

		 if(person.getName() == null && person.getEmail() == null && person.getBirthDate() == null && address.getPostalCode() == 0) {
			 throw new AppException("407");
		 }
		 
		 try {
		 
			 PersonService personService = new PersonService();
		 
			 boolean isUnique = personService.uniqueChecker(person);

			 if(isUnique == false) {
				 throw new AppException("413");
			 }

			 Connection connection = Connections.getConnection();
			 PreparedStatement personStatement = 
					 connection.prepareStatement(QueryStatement.insertPersonQuery);
			 PreparedStatement addressStatement = 
					 connection.prepareStatement(QueryStatement.insertAddressQuery, Statement.RETURN_GENERATED_KEYS);

			 addressStatement.setString(1, address.getStreet());
			 addressStatement.setString(2, address.getCity());
			 addressStatement.setLong(3, address.getPostalCode());
		 
			 int addressResult = addressStatement.executeUpdate();
		 
			 if(addressResult == 0) {
				 throw new AppException("Address Creation Failure");
			 } 
			 ResultSet result = addressStatement.getGeneratedKeys();
			 while(result.next()) {
				 address_id = result.getLong(1);
			 }

			 personStatement.setString(1, person.getName());
			 personStatement.setString(2, person.getEmail());
			 personStatement.setLong(3, address_id);
			 personStatement.setDate(4, person.getBirthDate());
			 
			 readPerson = personService.readPerson(address_id);
			 
			 if(readPerson != null) {
				 throw new AppException("414");
			 }

			 personResult = personStatement.executeUpdate();
		 
			 if(personResult == 0) {
				 deleteStatus = addressObject.deleteAddress(address_id);
				 throw new AppException("Person Creation Failure");
			 }
			 if(deleteStatus == 0) {
				 throw new AppException("Address Deletion Failure");
			 } else {
				 System.out.println("Person Creation and Address Creation Failure");
			 }

			 connection.close();
		 } catch(SQLException e) {
			 e.printStackTrace();
		 	}
		 return personResult;
	}
	
	public long insert(Person person) throws AppException {
			
		if(person.getName() == null && person.getEmail() == null && person.getBirthDate() == null ) {
			throw new AppException("407");
		}
			
		Connection connection = Connections.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertPersonQuery);
				
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setDate(4, person.getBirthDate());
				
			person.id = statement.executeUpdate();
				
			if(person.id == 0) {
				throw new AppException("407");
			}
			connection.close();
				
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return person.id;
	}
	*/
	
	public long create(Person person, Address address) throws Exception{
			
			long person_id = 0;
			long address_id = 0;
			long person_idTwo = 0;
			
			if(address != null) {
				 if(person.getName() == null || 
					person.getEmail() == null || 
					person.getBirthDate() == null || 
					address.getPostalCode() == 0) {
					 throw new AppException(ErrorCodes.FIELDS_ERROR);
				 }
				 
				 PersonService personService = new PersonService();
				 AddressService addressService = new AddressService();
				 long isUnique = personService.uniqueChecker(person);
				 if(isUnique != 0) {
					 throw new AppException(ErrorCodes.UNIQUE_EMAIL);
				 } else {
					 Connection connection = ConnectionService.initConnection();
					 
					 try {
						PreparedStatement insertPersonStatement = 
								connection.prepareStatement(QueryStatement.INSERT_PERSON_QUERY,java.sql.Statement.RETURN_GENERATED_KEYS);
						
						address_id = addressService.create(address);
						
						if(address_id != 0) {
							insertPersonStatement.setString(1,person.getName());
							insertPersonStatement.setString(2,person.getEmail());
							insertPersonStatement.setLong(3,address_id);
							insertPersonStatement.setDate(4,person.getBirthDate());
							
							long personStatus = insertPersonStatement.executeUpdate();
							
							ResultSet personResult = insertPersonStatement.getGeneratedKeys();
							while(personResult.next()) {
								person_id = personResult.getLong(1);
							}
							
							if(personStatus == 0) {
								addressService.deleteAddress(address_id);
								throw new AppException(ErrorCodes.PERSON_CREATION);
							} else {
								System.out.println("Person Creation Success");
							}
						} 
						ConnectionService.releaseConnection(connection);
					} catch (SQLException e) {
						e.printStackTrace();
					};
				 }
			} else {
				if(person.getName() == null || 
				   person.getEmail() == null || 
				   person.getBirthDate() == null) {
					throw new AppException(ErrorCodes.FIELDS_ERROR);
				}
				PersonService personService = new PersonService();
				long isUnique = personService.uniqueChecker(person);
				if(isUnique != 0) {
					 throw new AppException(ErrorCodes.UNIQUE_EMAIL);
				} else {
					Connection connectionTwo = ConnectionService.initConnection();
					try {
						PreparedStatement insertPersonStatementTwo = 
								connectionTwo.prepareStatement(QueryStatement.INSERT_PERSON_QUERY,java.sql.Statement.RETURN_GENERATED_KEYS);
						
						insertPersonStatementTwo.setString(1,person.getName());
						insertPersonStatementTwo.setString(2,person.getEmail());
						insertPersonStatementTwo.setLong(3,1);
						insertPersonStatementTwo.setDate(4,person.getBirthDate());
						
						long personStatusTwo = insertPersonStatementTwo.executeUpdate();
						
						ResultSet personResultTwo = insertPersonStatementTwo.getGeneratedKeys();
						while(personResultTwo.next()) {
							person_idTwo = personResultTwo.getLong(1);
						}
						
						if(personStatusTwo != 0) {
							System.out.println("Person Creation Success");
						}
						ConnectionService.releaseConnection(connectionTwo);
					} catch(SQLException e) {
						e.printStackTrace();
					}
				}
				return person_idTwo;
			}
			
			return person_id;
		}
	
	//Read Person
	/*
	public Person readPerson(long id) throws AppException {
		
		Person person = null;
		if(id == 0) {
			throw new AppException("409");
		}
		
		AddressService address = new AddressService();
		
		Address addressGot = address.readAddress(id);
		
		Connection connection = Connections.getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readPersonQuery);
			
			statement.setLong(1, id);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				person.id = result.getLong("id");
				person.name = result.getString("name");
				person.email = result.getString("email");
				person.address_id = addressGot.getId();
				person.birth_date = result.getDate("birth_date");
				person.created_date = result.getDate("created_date");
			}
			
			if(person == null) {
				throw new AppException("409");
			}
			connection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return person;
	}
	*/
	public List<Object> readPerson(Person personGot, boolean includeAddress) throws Exception {
		
		List<Object> objectList = new ArrayList<Object>();
		Address address = new Address(null, null, personGot.getId());
		Person person = new Person(null, null, null);
		
		if(personGot.getId() != 0) {
			Connection connection = ConnectionService.initConnection();
			
			try {
				PreparedStatement readPersonStatement = 
						connection.prepareStatement(QueryStatement.READ_PERSON_QUERY, Statement.RETURN_GENERATED_KEYS);
				
				readPersonStatement.setLong(1,personGot.getId());
				
				ResultSet result = readPersonStatement.executeQuery();
				
				while(result.next()) {
					person.setId(result.getLong("id"));
					person.setName(result.getString("name"));
					person.setEmail(result.getString("email"));
					person.setAddressId(result.getLong("address_id"));
					person.setCreatedDate(result.getDate("birth_date"));
					person.setBirthDate(result.getDate("created_date"));
				}
				
				ConnectionService.releaseConnection(connection);
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
			
			if(person.id != 0) {
				objectList.add(person);
			} else {
				throw new AppException(ErrorCodes.PERSON_READ);
			}
			
			if(includeAddress == true) {
				AddressService addressService = new AddressService();
				
				address = addressService.readAddress(person);
				
				if(address == null) {
					throw new AppException(ErrorCodes.ADDRESS_NOT_FOUND);
				}
				
				objectList.add(address);
			}
		}
		return Arrays.asList(person, address);
	}
	
	//ReadAll Person
	
	public List<Person> readAllPerson() throws Exception {
		
		List<Person> allPerson = new ArrayList<>();
		
		Person person = null;
		
		Connection connection = ConnectionService.initConnection();
		
		if(connection == null) {
			throw new AppException(ErrorCodes.CON_FAILURE);
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.READ_ALL_PERSON_QUERY);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				person.id = result.getLong("id");
				person.name = result.getString("name");
				person.email = result.getString("email");
				person.address_id = result.getLong("address_id");
				person.birth_date = result.getDate("birth_date");
				person.created_date = result.getDate("created_date");
				
				allPerson.add(person);
			}
			ConnectionService.releaseConnection(connection);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(allPerson == null) {
			throw new AppException(ErrorCodes.PERSON_READ);
		}
		
		return allPerson;
	}
	
	//Update Person
	
	public long update(Person person) throws Exception {
		
		int changes = 0;
			
		if(person.getId() == 0) {
			throw new AppException(ErrorCodes.PERSON_ID);
		}
			
		Connection connection = ConnectionService.initConnection();
			
		if(connection == null) {
			throw new AppException(ErrorCodes.CON_FAILURE);
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.UPDATE_PERSON);
				
			statement.setString(1,person.getName());
			statement.setString(2,person.getEmail());
			statement.setDate(3,person.getBirthDate());
			statement.setDate(4,person.getCreatedDate());
			statement.setLong(5,person.getId());
				
			changes = statement.executeUpdate();
			
			ConnectionService.releaseConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(changes == 0) {
			throw new AppException(ErrorCodes.PERSON_UPDATE);
		}
		
			
		return changes;
	}
	
	//Delete Person
	
	public long delete(long id) throws Exception {

		
		long changes = 0;
			
		if(id == 0) {
			throw new AppException(ErrorCodes.PERSON_ID);
		}
			
		Connection connection = ConnectionService.initConnection();
			
		if(connection == null) {
			throw new AppException(ErrorCodes.CON_FAILURE);
		}
			
		try {
			PreparedStatement statement = 
					connection.prepareStatement(QueryStatement.DELETE_PERSON);
			
			statement.setLong(1, id);
				
			changes = statement.executeUpdate(); 
			
			ConnectionService.releaseConnection(connection);
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		if(changes == 0) {
			throw new AppException(ErrorCodes.PERSON_DELETE);
		}
		
		return changes;
	}
	
	// Unique Checker

	public long uniqueChecker(Person person) throws Exception{
		long isUnique = 0;
		
		Connection connection = ConnectionService.initConnection();
		
		try {
			PreparedStatement uniqueStatement = 
					connection.prepareStatement(QueryStatement.UNIQUE_QUERY);
			
			uniqueStatement.setString(1,person.email);
			
			ResultSet result = uniqueStatement.executeQuery();
			
			while(result.next() ) {
				isUnique = result.getLong("id");
			}
			
			ConnectionService.releaseConnection(connection);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return isUnique;
	}
	
}
