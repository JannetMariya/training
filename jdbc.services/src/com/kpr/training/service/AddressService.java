package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.service.ConnectionService;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

/**
 * Problem Statement
 * 1.Perform CRUD Operations for Address Service
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public long insert(Address address);
 * 2.public Address readAddress(long id);
 * 3.public List<Address> readAllAddress();
 * 4.public int updateAddress(Address address, long id);
 * 5.public long deleteAddress(long id);
 * 
 * Jobs to be Done
 * 1.Create a Address
 * 2.Read an Address
 * 3.Read all Addresses
 * 4.Update an Address
 * 5.Delete an Address
 * @author UKSD
 *
 */

public class AddressService {
	
	public long addressId ;
	public int updateCount;
	public long deletedCount;
	
	//InsertAddress
	/*
	public long insert(Address address) throws Exception {
			
		try {
			if(address.getPostalCode() == 0) {
				throw new AppException("402");
			}
				
			Connection connection = Connections.getConnection();
				
			PreparedStatement statement = connection.prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY);
				
			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());
				
			addressId = statement.executeUpdate();
				
			if(addressId == 0) {
				throw new AppException("403");
			}
			connection.close();
				
		} catch(AppException e) {
			e.printStackTrace();
		}
	return addressId;
	}
	*/
	
	public long create(Address address) throws Exception {
		
		long address_id = 0;
		
		if(address.getPostalCode() == 0) {
			throw new AppException(ErrorCodes.POSTAL_ERROR);
		}
		
		Connection connection = ConnectionService.initConnection();
		
		PreparedStatement insertAddressStatement = 
				connection.prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY, java.sql.Statement.RETURN_GENERATED_KEYS);
		
		insertAddressStatement.setString(1, address.getStreet());
		insertAddressStatement.setString(2, address.getCity());
		insertAddressStatement.setLong(3, address.getPostalCode());
		
		long addressStatus = insertAddressStatement.executeUpdate();
		
		ResultSet result = insertAddressStatement.getGeneratedKeys();
		while(result.next()) {
			address_id = result.getLong(1);
		}
		
		if(address_id == 0) {
			throw new AppException(ErrorCodes.ADDRESS_ERROR);
		}
		ConnectionService.releaseConnection(connection);
		
		return address_id;
		
	}
	//ReadAddress
	
	public Address readAddress(Person person) throws Exception{
		
		Address address = null;
		
		try {
			if(person.getId() == 0) {
				throw new AppException(ErrorCodes.FIELD_EMPTY);
			}
			try {

				Connection connection = ConnectionService.initConnection();
				
				PreparedStatement statement = 
						connection.prepareStatement(QueryStatement.READ_ADDRESS_QUERY);
				
				statement.setLong(1, person.getId());
				
				ResultSet result = statement.executeQuery();
				
				while(result.next()) {
					address.street = result.getString("street");
					address.city = result.getString("city");
					address.postal_code = result.getLong("postal_code");
				}
				
				ConnectionService.releaseConnection(connection);
				
				if(address == null) {
					throw new AppException(ErrorCodes.ADDRESS_NOT_FOUND);
				}
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return address;
	}
	
	//ReadAlladdress
	
	public List<Address> readAllAddress() throws Exception {
		
		Address address = null;
		List<Address> addressList = new ArrayList<>();
		
		Connection connection = ConnectionService.initConnection();
		
		if(connection == null) {
			throw new AppException(ErrorCodes.CON_FAILURE);
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				address.street = result.getString("street");
				address.city = result.getString("city");
				address.postal_code = result.getLong("postal_code");
				
				addressList.add(address);
			}
			ConnectionService.releaseConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return addressList;
	}
	
	//UpdateAddress
	
	public int update(Address address, long id) throws Exception {
		int count = 0;
		try {
			
			if(address.getPostalCode() == 0) {
				throw new AppException(ErrorCodes.POSTAL_ERROR);
			}
			Connection connection = ConnectionService.initConnection();
			
			PreparedStatement statement = connection.prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY);
			
			statement.setString(1,address.getStreet());
			statement.setString(2,address.getCity());
			statement.setLong(3,address.getPostalCode());
			statement.setLong(4,id);
			
			count = statement.executeUpdate();
			
			if(count == 0) {
				throw new AppException(ErrorCodes.ADDRESS_ERROR);
			}
			ConnectionService.releaseConnection(connection);
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return count;
	}
		
	//DeleteAddress
		
	public long deleteAddress(long id) throws Exception {
			
		long deletedCount = 0;
		try {
			if(id == 0) {
				throw new AppException(ErrorCodes.FIELD_EMPTY);
			}
			
			Connection connection = ConnectionService.initConnection();
				
			PreparedStatement statement = connection.prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY);
				
			statement.setLong(1, id);
				
	
			deletedCount = statement.executeUpdate();
				
			if(deletedCount == 0) {
				throw new AppException(ErrorCodes.UPDATE_FAILURE);
			}
			ConnectionService.releaseConnection(connection);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return deletedCount;
	}
}
