package com.java.training.reflections.arrays;/*Requirment:
 * To write a java code to retrieve the added elements using the appropriate method.
 *
 * Entity:
 *      ArrayRetrieveElement
 * 
 * Jobs to be done:
 *    create a array using newintance mathod and initialize its size.
 *    add elements to array using setin method
 *    we can retrive elements of array using getint method.
 *    
 * Pseudo Code:
 * public class ArrayRetrieveElement { 
	public static void main(String[] args) 
	{ 

		int sizeOfArray = 3; 

		
		int[] intArray = (int[])Array.newInstance(int.class, sizeOfArray); 

	 

		System.out.println(Arrays.toString(intArray)); 

		
		System.out.println("Element at index 2: "+ Array.getInt(intArray, 2))
 * 
 * 
 * */import java.lang.reflect.Array; 
import java.util.Arrays; 

public class ArrayRetrieveElement { 
	public static void main(String[] args) 
	{ 

		int sizeOfArray = 3; 

		// Create an integer array using reflect.Array class and this is done using the newInstance() method 
		int[] intArray = (int[])Array.newInstance(int.class, sizeOfArray); 

		// Add elements from the array and this is done using the getInt() method 
		Array.setInt(intArray, 0, 10); 
		Array.setInt(intArray, 1, 20); 
		Array.setInt(intArray, 2, 30); 

		System.out.println(Arrays.toString(intArray)); 

		// Retrieve elements from the array and this is done using the getInt() method 
		System.out.println("Element at index 0: "+ Array.getInt(intArray, 0)); 
		System.out.println("Element at index 1: "+ Array.getInt(intArray, 1)); 
		System.out.println("Element at index 2: "+ Array.getInt(intArray, 2)); 
	} 
} 
