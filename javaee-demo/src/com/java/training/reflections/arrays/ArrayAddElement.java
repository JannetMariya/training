package com.java.training.reflections.arrays;
/*Requirment:
 * To Create a java array using reflect.array class and add elements using the appropriate method.
 * 
 *Entity:
 *   ArrayAddElement 
 *   
 *Jobs To Be Done:
 *  To create a java first declare a class and get sze of array.
 *  Then create integer array using newinstance method.
 *  add element to array using setin method.
 *  
 * Pseudo Code:
 * 
 * public class ArrayAddElement { 
	public static void main(String[] args) 
	{ 

		int sizeOfArray = 3; 

		
		int[] intArray = (int[])Array.newInstance(int.class, sizeOfArray); 

		

		System.out.println(Arrays.toString(intArray)); 
*/
import java.lang.reflect.Array; 
import java.util.Arrays; 

public class ArrayAddElement { 
	public static void main(String[] args) 
	{ 

		int sizeOfArray = 3; 

		// Create an integer array using reflect.Array class this is done using the newInstance() method 
		int[] intArray = (int[])Array.newInstance(int.class, sizeOfArray); 

		// Add elements into the array and this is done using the setInt() method 
		Array.setInt(intArray, 0, 10); 
		Array.setInt(intArray, 1, 20); 
		Array.setInt(intArray, 2, 30); 

		System.out.println(Arrays.toString(intArray)); 
	} 
} 
