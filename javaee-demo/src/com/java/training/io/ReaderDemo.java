/*
Requirement:
    To read the file using Reader

Entity:
    ReaderDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1.An exception is thrown
    2.Set the path of the file and declare the variable as i
        2.1 In while loop,read the file until loop gets false
        2.2 Print the result
    3.Use close() method,to close the reading file.

Pseudocode:
public class ReaderDemo {
           // throws Exception
	public static void main(String[] args)throws Exception {    
		          FileReader fr=new FileReader("E:\\example.txt"); //set the path to read the file   
		          int i;    
		          while((i=fr.read())!=-1)    
		          System.out.print((char)i);    
		          fr.close();    
		    }    
}
*/

package com.java.training.io;

import java.io.FileReader;  

public class ReaderDemo {

	public static void main(String[] args)throws Exception {    
		          FileReader fr=new FileReader("C:\\Users\\Jannet\\Documents\\Sample.txt");    
		          int i;    
		          while((i=fr.read())!=-1)    
		          System.out.print((char)i);    
		          fr.close();    
		    }    
}