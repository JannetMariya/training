/*
Requirement:
    To read the file using inputstream

Entity:
    FileDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1.In try block,Create file input stream 
        1.1 In First,Set the path to read the file and initialize i=0 which type is int
        1.2 In while condition,read the file until it gets false 
        1.3 Print the result
    2.After read the file,Use close()method is used to close the file.

Pseudocode:
public class FileDemo {

	public static void main(String[] args) {
                //create try and catch block
		 try {
		        FileInputStream fis = new FileInputStream("E:\\example.txt");  //Fix the path to read the file 
		        int i = 0;
				while((i=fis.read())!= -1) {

				System.out.println((char)i);}
		        fis.close();
		        } catch(Exception e) {
		              System.out.println(e);
		        }
		  }
}
*/

package com.java.training.io;

import java.io.FileInputStream;

public class FileDemo {
	

	public static void main(String[] args) {
		 try {
		        FileInputStream fis = new FileInputStream("C:\\Users\\Jannet\\Documents\\Sample.txt");
		        int i = 0;
				while((i=fis.read())!= -1) {

				System.out.println((char)i);}
		        fis.close();
		        } catch(Exception e) {
		              System.out.println(e);
		        }
		  }
}