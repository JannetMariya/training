/*
Requirements:
    To write a string content using Writer

Entities:
    WriterDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1.In main method,try and catch block is used
        1.1 Set the path of the file and create a string named as content
        1.2 Write the content to the file. 
        1.3 Use close() method,to close the file.
    2.print the output.

Pseudocode:
public class WriterDemo {
   public static void main(String[] args) {
//create try and catch block
		 try {  
	            Writer w = new FileWriter("E:\\reader.txt"); //set the path  
	            String content = "A small key opens big doors";  //create a string
	            w.write(content);  
	            w.close();  
	            System.out.println("Done");  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	}

}


*/


package com.java.training.io;

import java.io.Writer;
import java.io.FileWriter;

public class WriterDemo {
    public static void main(String[] args) {
        try {  
	             Writer w = new FileWriter("C:\\\\Users\\\\Jannet\\\\Documents\\\\Sample.txt");  
	             String content = "A small key opens big doors";  
	             w.write(content);  
	             w.close();  
	             System.out.println("Done");  
	        } catch (Exception e) {  
	             e.printStackTrace();  
	        }  
	}

}