package com.java.training.nio;/*
NIO
===
1.Create two files named source and destination .Transfer the data from source file to destination file 
using nio filechannel.

--------------------------------WBS---------------------------------------

1.Requirements:
    - Program to transfer the data from source file to destination file 
using nio filechannel.
    
2.Entities:
    - ChanneTransfer
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Store the array of file path to transfer files. 
    2.Store the file path to distination files.
    3.Invoke FileOutputStream class pass argument as new File with destination file string.
    4.Invoke WritableByteChannel class getChannel method to get channel path.
    5.For loop Get the source file
        5.1)Transfer the file content source to destination using transferTo method
        5.2)Close inputChannel and fis using close method.
    
Pseudo Code:
''''''''''''
public class ChanneTransfer {
	public static void main(String[] argv) throws Exception {
		String[] inputFiles = new String[] { "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/source.txt"};
		String outputFile = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/destination.txt";
		FileOutputStream fos = new FileOutputStream(new File(outputFile));
		WritableByteChannel targetChannel = fos.getChannel();

		for (int i = 0; i < inputFiles.length; i++) {
			FileInputStream fis = new FileInputStream(inputFiles[i]);
			FileChannel inputChannel = fis.getChannel();

			inputChannel.transferTo(0, inputChannel.size(), targetChannel);
			inputChannel.close();
			fis.close();
		}
		System.out.println("The data successfully transfer from source file to destination file");
		targetChannel.close();
		fos.close();
	}
}
--------------------------------Program Code---------------------------------------
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

public class ChanneTransfer {
	public static void main(String[] argv) throws Exception {
		// Input files
		String[] inputFiles = new String[] { "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/source.txt"};

		// Files contents will be written in these files
		String outputFile = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/destination.txt";

		// Get channel for output file
		FileOutputStream fos = new FileOutputStream(new File(outputFile));
		WritableByteChannel targetChannel = fos.getChannel();

		for (int i = 0; i < inputFiles.length; i++) {
			// Get channel for input files
			FileInputStream fis = new FileInputStream(inputFiles[i]);
			FileChannel inputChannel = fis.getChannel();

			// Transfer data from input channel to output channel
			inputChannel.transferTo(0, inputChannel.size(), targetChannel);

			// close the input channel
			inputChannel.close();
			fis.close();
		}
		System.out.println("The data successfully transfer from source file to destination file");
		// finally close the target channel
		targetChannel.close();
		fos.close();
	}
}
