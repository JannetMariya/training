package com.java.training.mapping;
/*
 1)Write a Java program to copy all of the mappings from the specified map to another map?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to copy all of the mappings from the specified map to another map.
2.Entities
   - SpecifiedKey
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as SpecifiedKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called colors.
   3.Adding the values using put() method and creating another hashMap colors2 for copying from colors to colors2 using putAll() method.
   4.Printing the copied hashMap colors2.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/


/*
4) Write a Java program to get the portion of a map whose keys range from a given key to another key?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
   - GetPortion
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String called colors.
   3.Selecting the portion of the TreeMap using subMap() method.
   3.Printing the Sub map key values.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/


/*
4) Write a Java program to get the portion of a map whose keys range from a given key to another key?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
   - GetPortion
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String called colors.
   3.Selecting the portion of the TreeMap using subMap() method.
   3.Printing the Sub map key values.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/


import java.util.HashMap;

public class SpecifiedMap {

   public static void main(String args[]) {
	   
	  //Creating hashMap 
      HashMap<Integer, String> colors = new HashMap<Integer, String>();

      /*Adding elements to HashMap*/
      colors.put(4, "Blue");
      colors.put(2, "Green");
      colors.put(3, "Red");
      colors.put(5, "Yellow ");
      colors.put(1, "Orange");
      
      HashMap<Integer, String> colors2 = new HashMap<Integer, String>();
      
      
      //Copy all of the mappings from the specified map to another map
      colors2.putAll(colors);
      System.out.println(colors2);
   }
}