package com.java.training.mapping;
/*
4) Write a Java program to get the portion of a map whose keys range from a given key to another key?


----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
   - GetPortion
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String called colors.
   3.Selecting the portion of the TreeMap using subMap() method.
   3.Printing the Sub map key values.


-----------------------------------------------Program--------------------------------------------------------------------------------
*/


import java.util.TreeMap; 

public class GetPortion {

	public static void main(String[] args) {
		
		 //Creating TreeMap and SortedMap
	      TreeMap<Integer, String> colors = new TreeMap<Integer, String>();
	      
	      /*Adding elements to HashMap*/
	      colors.put(34, "Blue");
	      colors.put(20, "Green");
	      colors.put(13, "Red");
	      colors.put(5, "Yellow ");
	      colors.put(1, "Orange");
	     
		  System.out.println("-------------Sub map key-values------------------- \n" + colors.subMap(20, 40));
	}

}
