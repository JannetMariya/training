package com.java.training.reflection.fields;

import java.lang.reflect.Field;

public class User {
    public int id;
    private String name;
 
    public User(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws ClassNotFoundException {
     this.name = name;
     Field[] fields = User.getFields();
     for (Field field : fields) {
         System.out.println(field.getName());
     } }

	private static Field[] getFields() {
		return null;
	}
}
