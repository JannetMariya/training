package com.java.training.serialization;
/*
5) Write java programs for stream tokenizer methods :-
   5.1) commentChar() method
   5.2) eollsSignificant() method
   5.3) lineno() method
   5.4) lowerCaseMode() method

--------------------------------------WBS-----------------------------------------
1.Requirement:
   - Program to stream tokenizer methods
         * commentChar() method     
         * eollsSignificant() method
         * lineno() method          
         * lowerCaseMode() method  

2.Entity:
   - StreamTokenMethods 

3.Method Signature:
   - public static void main(String [] args)
   - commentCharMethod()
   - eollsSignificantMethod()
   - linenoMethod()          
   - lowerCaseModeMethod()
4.Jobs to be done:
    1.Invoke StreamTokenMethods class methods.
          1.1)commentCharMethod
          1.2)eollsSignificantMethod
          1.3)linenoMethod
          1.4)lowerCaseModeMethod
   2.Each methods create FileReader class with argument of file path
   3.Read by BufferedReader class uses methods in methods
          3.1)commentCharMethod
             =>commentChar() method
          3.2)eollsSignificantMethod
             =>eollsSignificant () method
          3.3)linenoMethod
             =>linenoMethod() method 
          3.4)lowerCaseModeMethod
             =>lowerCaseMode() method
   4.Each methods prints Words and Number separately using switch case.
   
Psudocode:
''''''''''
public class StreamTokenMethods {
	
	public void commentCharMethod() throws IOException {
		
	public void eollsSignificantMethod() throws IOException {
	
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_EOL: 
                System.out.println("End of Line encountered."); 
                break; 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
            } 
        } 
        
	}
	
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_EOL: 
                System.out.println(""); 
                System.out.println("Line No. : " + token.lineno()); 
                break; 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
            } 
        } 
    } 
	public void lowerCaseModeMethod() throws IOException {
		FileReader reader = new FileReader("C:\Users\Jannet\Documents\Sample.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread);
        boolean arg = true; 
        token.lowerCaseMode(arg); 
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break;
            } 
        } 
	}
	public static void main(String[] args) throws IOException {
		StreamTokenMethods tokenMethods = new StreamTokenMethods();
		System.out.println("----Using commentCharMethod----");
		tokenMethods.commentCharMethod();
		System.out.println("----Using eollsSignificantMethod----");
		tokenMethods.eollsSignificantMethod();
		System.out.println("----Using linenoMethod----");
		tokenMethods.linenoMethod();
		System.out.println("----Using  lowerCaseModeMethod----");
		tokenMethods.lowerCaseModeMethod();
	}
}
--------------------------------------Program Code-----------------------------------------*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

public class StreamTokenMethods {
	
	public void commentCharMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/santh/eclipse-workspace/JavaEE-Demo/serialization/readtoken.txt");
		BufferedReader bufferread = new BufferedReader(reader);
		StreamTokenizer token = new StreamTokenizer(bufferread);
		// Use of commentChar() method
		token.commentChar('a');
		int t;
		while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) {
			switch (t) {
			case StreamTokenizer.TT_NUMBER:
				System.out.println("Number : " + token.nval);
				break;
			case StreamTokenizer.TT_WORD:
				System.out.println("Word : " + token.sval);
				break;
			}
		}
	}
	public void eollsSignificantMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/santh/eclipse-workspace/JavaEE-Demo/serialization/readtoken.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread); 
  
        boolean arg = true; 
        // Use of eolIsSignificant() method 
        token.eolIsSignificant(arg); 
        // Here the 'arg' is set true, so EOL is treated as a token 
  
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_EOL: 
                System.out.println("End of Line encountered."); 
                break; 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
  
            } 
        } 
        
	}
	public void linenoMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/santh/eclipse-workspace/JavaEE-Demo/serialization/readtoken.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread); 
          
        token.eolIsSignificant(true); 
        // Use of lineno() method  
        // to get current line no. 
        System.out.println("Line Number:" + token.lineno()); 
  
        token.commentChar('a'); 
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_EOL: 
                System.out.println(""); 
                System.out.println("Line No. : " + token.lineno()); 
                break; 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
  
            } 
        } 
    } 
	public void lowerCaseModeMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/santh/eclipse-workspace/JavaEE-Demo/serialization/readtoken.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread); 
  
        /* Use of lowerCaseMode() method to 
           Here, the we have set the Lower Case Mode ON 
        */
        boolean arg = true; 
        token.lowerCaseMode(arg); 
  
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
  
            } 
        } 

	}
	public static void main(String[] args) throws IOException {
		StreamTokenMethods tokenMethods = new StreamTokenMethods();
		System.out.println("----Using commentCharMethod----");
		tokenMethods.commentCharMethod();
		System.out.println("----Using eollsSignificantMethod----");
		tokenMethods.eollsSignificantMethod();
		System.out.println("----Using linenoMethod----");
		tokenMethods.linenoMethod();
		System.out.println("----Using  lowerCaseModeMethod----");
		tokenMethods.lowerCaseModeMethod();
	}
}
