/*REQUIRMENT:
 *  To write a java  program to display  escape sequence in java
		"Escaping characters"
   ENTITY:
      EscapeSequenceC
      
   JOBS TO BE DONE:
   
       Declare and name a class .
       Initialize the given string to a new variable and use the respective escape sequence to get in the given format.
       then print the string.
       
    PSEUDO CODE:
      public class EscapeSequenceA{
public static void main(String[] args) {
    String myFavoriteBook = new String("\"Escaping characters\""); 
       */
package com.java.training.regex;

public class EscapeSequenceC {
   public static void main(String[] args) {
       System.out.println("\"Escaping characters\"");
   }
}