package com.java.training.regex.quantifier;
/*
Requirement:
    To write a code for java regex quantifiers
    
Entity:
    RegexQuantifiers

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a String variable text and assign the value 
    2. Use the quantifiers and print the result.
    
Pseudo code:

class RegexQuantifiers {
    
   
        String text = "";
        Pattern pattern = Pattern.compile();
        Matcher matcher = pattern.matcher();
        int i = 0;
        while (matcher.find()) {
            i++;
    // print the result;
    }
}

*/


import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegexQuantifier {
    
    public static void main(String[] args) {
        String text = "MSMSMSMSMSSSSMMMS";
        Pattern pattern = Pattern.compile("M?S");
        Matcher matcher = pattern.matcher(text);
        
        int i = 0;
        while (matcher.find()) {
            i++;
            System.out.println(i + matcher.group());
        }
        
    }
}