/*
Requirement:
    To write a code for the validation of given mail ids.

Entity:
    EmailValidation

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a list of Strings that contains the list of mail id of the persons.
    2. Check for the valid mail id of the person.
        2.1) Based upon the result print the result in boolean value.
        
Pseudo code:
class EmailValidation {
    
    public static void main(String[] args) {
        List<String> emailId = new ArrayList<>();
        // add the emailId
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        
        for (String email : emailId) {
            Matcher matcher = pattern.matcher(email);
            // print true or false based on the emails.
       }
   }
}
 
*/
package com.java.training.regex.quantifier;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
    
    public static void main(String[] args) {
        List<String> emailId = new ArrayList<>();
        emailId.add("harshini@gmail.com");
        emailId.add("writer0301@gmail.com");
        emailId.add("@gmail.com");
        emailId.add("18ec055@kpriet.ac.in");
        
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        
        for (String email : emailId) {
            Matcher matcher = pattern.matcher(email);
            System.out.println(email + " : " + matcher.matches());
        }
        
    }

}