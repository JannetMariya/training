package com.java.training.properties;
/*
1.Write a program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.

----------------------------------WBS--------------------------------------

1.Requirement:
  - Program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.

2.Entity:
  - PropertiesDemo

3.Method Signature:
  - public static void main(String[] args)

4.Jobs to be done:
    1.Create an object properties for Properties class.
    2.Set some properties with key and values to properties object.
    3.Display all property using Iterator object with keySet.
    4.Using Stream to display all property in entrySet.

Pseudo Code:
'''''''''''
public class PropertiesExample {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty("username", "Santheesh");
        properties.setProperty("email", "santheesh62@gmail.com");
        properties.setProperty("password", "16092000santh");
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }
        properties.entrySet().stream().forEach(System.out::println);
    }
}
----------------------------------Program Code--------------------------------------
*/


import java.util.Iterator;
import java.util.Properties;

public class PropertiesExample {

    public static void main(String[] args) {

        // Creating an object of Properties class
        Properties properties = new Properties();

        properties.setProperty("username", "Jannet");
        properties.setProperty("email", "janeet62@gmail.com");
        properties.setProperty("password", "16092000jannet");

        // Using Iterator to display key and values on properties
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        // Using entrySet method of HashTable to get the key value pair
        // Using streams forEach to display key and values on properties
        properties.entrySet().stream().forEach(System.out::println);
    }

}
