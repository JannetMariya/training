package com.java.training.exceptionhandling;
/*Write a program ListOfNumbers (using try and catch block).
 -----------------------------------------WBS---------------------------------------
 1.  Requirement:
   - Write a program ListOfNumbers (using try and catch block).
 2.Entity:
   - ListOfNumbers
   - MainList
 3.Function Declaration:
   - public void writeList()
   - public static void main(String[] args)
 4.Jobs to be done:
   1. Creating a class ListOfNumbers and declared an array of integers arrayOfNumbers of size 10.
   2. A method writeList() is created.
   3. Declare and Initialize the array value.
   4. Execute the try block. 
   5. When an exception occurs in try block ,then go to the catch block and an exception is thrown to the first catch block.
   6. The first catch block does not handle an IndexOutOfBoundsException, so it is passed to the next catch block.
   7. The IndexOutOfBoundsException occurs because the array bounds for arrayOfNumbers is 0 to 9.
   8. Display the Exception. 
   
 5.Pseudo Code:
 	class ListOfNumbers {
  public int[] arrayOfNumbers = new int[10];
  public void writeList() {
    try {
      arrayOfNumbers[10] = 5;
    } catch (Exception e1) {
      System.out.println(" " + e1.getMessage());
    } catch (Exception e2) {
      System.out.println("" + e2.getMessage());
    }
  }
  public static void main(String[] args) {
	  ListOfNumbers list = new ListOfNumbers();
      list.writeList();
  }
}

 	 
-----------------------------------------Program---------------------------------------          
 */


class ListOfNumbers {
  public int[] arrayOfNumbers = new int[10];
  public void writeList() {
    try {
      arrayOfNumbers[10] = 5;
    } catch (NumberFormatException e1) {
      System.out.println("NumberFormatException => " + e1.getMessage());
    } catch (IndexOutOfBoundsException e2) {
      System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
    }
  }
  public static void main(String[] args) {
	  ListOfNumbers list = new ListOfNumbers();
      list.writeList();
  }
}
