package com.java.training.stackqueue;
/* Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of queue
  -> print the elements using Stream 
---------------------------------------------------------WBS---------------------------------------------------
1.Requirements :
  - Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

2.Entities:
  - QueueDemo 
3.Function Declaration:
  - linkedList()
  - priority
  - public static void main(String [] args)
4.Job to be done :  
   1.Create a class name called QueueDemo and declare main method
   2.In the main method creating object for class and invoke the two methods.
   3.In the linkedList() method creating Queue with generic String type with initailse Linkedlist 
and creating stream for displaying the Stack elements.
   4.Adding 5 values to Queue using add() method and using remove() method removing value specified value.
   5.Search the specified value using search() method and find size of stack using size() method.
   6.Using contains() method finding the given value is in the Queue or not and using size() method finding size of queue.
   7.Printing the queue values using stream and for each value.
   8.In the priority() method creating queue with initailse PriorityQueue and also creating stream to print the queue values.
   9.Adding the values to queue using add() method and finding specified value using contains() method.
   10.Finding the size using size() method and printing the PriorityQueue values 
  
5.Pseudo Code: 
	public class QueueDemo{
	public void linkedList() {
		 Queue<String> cars = new LinkedList<String>();
	        Stream<String> stream = cars.stream();
	        cars.add
	        
	        System.out.println(cars.contains(""));
	        System.out.println(cars.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	
	public void priority() {
		 Queue<String> cars = new PriorityQueue<String>();
	        Stream<String> stream = cars.stream();
	        cars.add()
	        
	        cars.remove();
	        System.out.println(cars.contains("Honda"));
	        System.out.println(cars.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("-----------------Linked List----------------------------");
    	queue.linkedList();
    	System.out.println("-----------------Priority----------------------------");
    	queue.priority();
    }
}

 ----------------------------------------------------Program Code---------------------------------------------------
*/
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;
public class QueueDemo{
	public void linkedList() {
		 Queue<String> cars = new LinkedList<String>();
	        Stream<String> stream = cars.stream();
	        cars.add("BMW");
	        cars.add("Mazda");
	        cars.add("Tesla");
	        cars.add("Lexus");
	        cars.add("Meclaren");
	        cars.remove();
	        
	        System.out.println(cars.contains("Tesla"));
	        System.out.println(cars.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> cars = new PriorityQueue<String>();
	        Stream<String> stream = cars.stream();
	        cars.add("BMW");        
	        cars.add("Mazda");
	        cars.add("Tesla");
	        cars.add("Lexus");
	        cars.add("Meclaren");
	        cars.remove();
	        System.out.println(cars.contains("Honda"));
	        System.out.println(cars.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("-----------------Linked List----------------------------");
    	queue.linkedList();
    	System.out.println("-----------------Priority----------------------------");
    	queue.priority();
    }
}