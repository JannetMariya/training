package com.java.training.stackqueue;
/* Consider a following code snippet
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    

  what will be output and complete the code.
---------------------------------------------------------WBS---------------------------------------------------
1.Requirements :
  - what will be output and complete the code.
2.Entities:
  - QueueSnippet 
3.Function Declaration:
  - public static void main(String [] args)
4.Job to be done :  
   1.Create a class name QueueSnippet and declare main method
   2.In the main creating the Queue with generic String type and adding values using add() method.
   3.To print the peek values poll() method to get values and using peek() printing the peek value.
   
 5.Pseudo Code:
 
 public class QueueSnippet{
    public static void main(String [] args) {
        Queue<String> bikes = new PriorityQueue<String>();
        bikes.add
 	     System.out.println()
 ----------------------------------------------------Program Code---------------------------------------------------
*/
import java.util.PriorityQueue;
import java.util.Queue;
public class QueueSnippet{
    public static void main(String [] args) {
        Queue<String> bikes = new PriorityQueue<String>();
        bikes.add("KTM");
        bikes.add("Honda");
        bikes.add("Duke");
        bikes.add("R15");
        bikes.add("FZ");
        bikes.add("Royal Enfield");
        bikes.poll();
        System.out.println(bikes.peek());  
    }
}