package com.java.training.collections.exercise;
/* create an array list with 7 elements, and create an empty linked list add all elements of 
 the array list to linked list ,traverse the elements and display the result
 

Requirements:
   * array list with values
Entities
   * ArrayToLinkedList
Function Declaration
   -none-
Jobs to be done
    * Create a class name ArrayToLinkedList
    * put public static void main
    * Create a array list name car
    * Add 7 values in the list
    * Create linked list name cars
    * add all values in array list to linked list
    * using iterator print all values
Pseudo Code:
public class ArrayToLinkedList {
	public static void main(String [] args) {
		List<String> car = new ArrayList<String>();
		car.add(" "); 	//Add 7 values in the list
		
		List<String> cars = new LinkedList<String>();
        while(car.size() > 0) {
            cars.add(car.remove(0));
            }
		
        Iterator<String> iter = cars.listIterator(); 
       
        while(iter.hasNext()){ 
           System.out.println(iter.next()); 
*
*/

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

public class ArrayToLinkedList {
	public static void main(String [] args) {
		List<String> car = new ArrayList<String>();
		car.add("BMW"); 	//Add 7 values in the list
		car.add("Mazda");
		car.add("Lexus");
		car.add("Audi");
		car.add("Mecleran");
		car.add("RR");
		car.add("Dodge");
		System.out.println("Elements in the array List : "+car);
		List<String> cars = new LinkedList<String>();
        while(car.size() > 0) {
            cars.add(car.remove(0));
            }
		System.out.println("Elements in the Linked List : "+cars);
        Iterator<String> iter = cars.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iter.hasNext()){ 
           System.out.println(iter.next()); 
        } 
	}

}
