package com.java.training.collections.exercise;
/* 6. LIST CONTAINS 10 STUDENT NAMES
    krishnan, abishek, arun,vignesh, kiruthiga, murugan,
    adhithya,balaji,vicky, priya and display only names starting with 'A'.
    
Requirements:
   *lsit with values
Entities
   * StudentList
Function Declaration
   -none-
Jobs to be done
    * Create a class name StudentList
    * put public static void main
    * Create a list name student
    * Add 10 values in the list
    * display only names starting with 'A'
Pseudo Code:
  public class StudentList {
	public static void main(String[] args) {
	List<String> student = new ArrayList<String>();
	student.add("");
	for(String i : student) {
	    if(i.startsWith("a")) System.out.println(i);
	    }
  
    */
import java.util.ArrayList;
import java.util.List;


public class StudentList {
	public static void main(String[] args) {
	List<String> student = new ArrayList<String>();
	student.add("krishnan"); 	//Add 10 values in the list
	student.add("abishek");
	student.add("arun");
	student.add("vignesh");
	student.add("kiruthiga");
	student.add("murugan");
	student.add("adhithya");
	student.add("balaji");
	student.add("vicky");
	student.add("priya");
	for(String i : student) {
	    if(i.startsWith("a")) System.out.println(i);
	    }
    }
 }

