package com.java.training.collections.exercise;
/*8 districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy  
     to be converted to UPPERCASE.
Requirements:
   * List with values
Entities
   * ConvertIntoUpperCase
Function Declaration
   -none-
Jobs to be done
    * Create a class name ConvertIntoUpperCase
    * put public static void main
    * Create a list name city
    * Add 8 values in the list
    * converted to UPPERCASE
    * Print the list
Pseudo Code:
public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add(" "); 
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }
      
     */
import java.util.ArrayList;
import java.util.List;

public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add("Madurai"); 	//Add 8 values in the list
	city.add("Coimbatore");
	city.add("Theni");
	city.add("Chennai");
	city.add("Karur");
	city.add("Salem");
	city.add("Erode");
	city.add("Trichy ");
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }
    }
}
