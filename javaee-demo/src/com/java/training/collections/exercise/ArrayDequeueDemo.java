package com.java.training.collections.exercise;
/* 4. use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast()
 methods to store and retrieve elements in ArrayDequeue.
Requirements:
   * Dequeue with values
Entities
   * ArrayDequeueDemo
Function Declaration
   -none-
Jobs to be done
    * Create a class name ArrayDequeueDemo
    * put public static void main
    * Create a array list name car
    * Add 7 values using addFirst(),addLast(),add methods in the dequeue
    * print peek values using peekFirst(),peekLast() methods in the dequeue
    * remove element using pollFirst(),pollLast(),removeFirst(),removeLast() methods in
    the dequeue
    * Print the dequeue
Pseudo Code:
public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> car = new LinkedList<String>();
		car.addFirst(" ")
		car.add("");
        car.addLast(" ");
        System.out.println("Peek First Value: "+car.peekFirst()); //peek first value
		System.out.println("Peek Last Value: "+car.peekLast()); //peek last value
		car.pollFirst();  //poll first value
		car.pollLast();   //poll last value
		car.removeFirst();  //remove first value
		car.removeLast();   //remove last value
		System.out.println(car);
 */

import java.util.LinkedList;
import java.util.Deque;
public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> car = new LinkedList<String>();
		car.addFirst("Meclaren");  //add at first
		car.add("BMW");
		car.add("Mazda");
		car.add("Lexus");
		car.add("Audi");
		car.addLast("Bugati");   //add at last
		System.out.println("Peek First Value: "+car.peekFirst()); //peek first value
		System.out.println("Peek Last Value: "+car.peekLast()); //peek last value
		car.pollFirst();  //poll first value
		car.pollLast();   //poll last value
		car.removeFirst();  //remove first value
		car.removeLast();   //remove last value
		System.out.println(car);
	}

}
