package com.java.training.collections.regex;
/*create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit

Requirements:
   * A string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
Entities
   * CreatePatten
Function Declaration
   -none-
Jobs to be done
 * Create a Class name CreatePatten
 * Put public static void main
 * Create A string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
 * Create a patten and put a patten
     =>^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$
 *Create and Using Matcher and also with if condition print the result   
 
Pseudo Code:

public class CreatePattern {
	public static void main (String [] args) {
		String a = " ";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(a);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      print("Match found");
	    } else {
	      print("Match not found");
	    }
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class CreatePattern {
	public static void main (String [] args) {
		String a = "Lamb0gini";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(a);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      System.out.println("Match found");
	    } else {
	      System.out.println("Match not found");
	    }
	}
}
