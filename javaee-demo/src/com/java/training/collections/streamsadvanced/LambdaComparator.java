package com.java.training.collections.streamsadvanced;

import java.util.List;

/**
 * Problem Statement
 * 1. lambda
 *   - sort the roster list based on the person's age in descending order using comparator
 *   
 * Requirement
 * 1. lambda
 *   - sort the roster list based on the person's age in descending order using comparator
 *   
 * Entity
 * 1.LmbdaComparator
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store it in persons list.
 * 2.Create a lambda expression to compare each person's age.
 * 3.print the person list.
 * 
 * 
 * Pseudo Code
 * class LambdaComparator {
 *	
 *	public static void main(String[] args) {
 *		
 *		List<Person> persons = Person.createRoster();
 *		
 *		persons.sort((Person person1, Person person2) -> person2.getAge()-person1.getAge()); 
 *		
 *		persons.forEach(person -> System.out.println(person.getName()));
 *		
 *	}
 *
 *}
 * 
 *
 */

public class LambdaComparator {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		persons.sort((Person person1, Person person2) -> person2.getAge()-person1.getAge());
		
		persons.forEach(person -> System.out.println(person.getName()));
		
	}

}
