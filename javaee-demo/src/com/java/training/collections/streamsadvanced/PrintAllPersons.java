package com.java.training.collections.streamsadvanced;

import java.util.List;

/**
 * Problem Statement
 * 1.forEach
 *   - Print all the persons in the roster using java.util.Stream<T>#forEach
 *   
 * Requirement
 * 1.forEach
 *   - Print all the persons in the roster using java.util.Stream<T>#forEach
 *   
 * Entity
 * 1.PrintAllPersons
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Print each person name from the list.
 * 
 * Pseudo Code
 * 
 * class PrintAllPersons {
 *	
 *	public static void main(String[] args) {
 *		
 *		List<Person> persons = Person.createRoster();
 *		
 *		persons.stream()
 *			   .forEach(person -> System.out.println(person.getName()));
 *		
 *	} 
 *
 *}
 * 
 */

public class PrintAllPersons {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		persons.stream()
			   .forEach(person -> System.out.println(person.getName()));
		
	}

}
