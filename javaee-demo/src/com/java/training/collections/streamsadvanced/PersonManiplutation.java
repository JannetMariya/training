package com.java.training.collections.streamsadvanced;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

/**
 * Problem Statement
 * 1.- Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
 *   - Print the number of persons in roster List after the above addition.
 *   - Remove the all the person in the roster list
 *   
 * Requiremen
 *  1.- Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
 *   - Print the number of persons in roster List after the above addition.
 *   - Remove the all the person in the roster list
 * 
 * Entity
 * 1.PersonManipluatition
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method from Person class and store it in persons list.
 * 2.Create a list called newRoster and add the person in newRoster list.
 * 3.Add all person from the newRoster list to persons list.
 * 4.Print the persons list and persons list. 
 * 5.Store the number of persons in the persons list to totalPerson and print it.
 * 6.Remove all the person in the persons list and print the list.
 * 7.Print the size of the persons list.
 * Pseudo Code
 * public class PersonManiplutation {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            " ",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            " "));
 *persons.addAll(newRoster);
        
        persons.forEach(person -> System.out.println(person.getName()));
		
		int totalPerson = persons.size();
		
        System.out.println(totalPerson);
        
        persons.removeAll(persons);
        System.out.println(persons.size());
 *
 */

public class PersonManiplutation {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        
        persons.addAll(newRoster);
        
        persons.forEach(person -> System.out.println(person.getName()));
		
		int totalPerson = persons.size();
		
        System.out.println(totalPerson);
        
        persons.removeAll(persons);
        System.out.println(persons.size());
	}

}
