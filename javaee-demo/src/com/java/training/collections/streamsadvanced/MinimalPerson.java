package com.java.training.collections.streamsadvanced;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Problem Statement
 * 1.Collectors
 *   - Write a program to collect the minimal person with name and email address from the Person class using java.util.Stream<T>#map API as List
 *   
 * Requirement
 * 1.Collectors
 *   - Write a program to collect the minimal person with name and email address from the Person class using java.util.Stream<T>#map API as List
 * 
 * Entity
 * 1.MinimalPerson
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method from the Person and store it in persons list.
 * 2.Check for each person whether the person's name and email is not null 
 * 3.Store the persons in verifiedPersons
 * 4.Print the person name from the verifiedPersons.
 * 
 * Pseudo Code
 * 
 *	class MinimalPerson {
 *		
 *		public static void main(String[] args) {
 *			
 *			List<Person> persons = Person.createRoster();
 *			
 *			List<Person >verifiedPersons = persons.stream()
 *												  .filter(person -> person.getName() != null)
 *												  .filter(person -> person.getEmailAddress() != null)
 *												  .map(person -> person)
 *												  .collect(Collectors.toList());
 *			verifiedPersons.stream()
 *						   .forEach(person -> System.out.println(person.getName()));
 *			
 *		}
 *	
 *	}
 * 
 * 
 * 
 *
 */

public class MinimalPerson {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		List<Person >verifiedPersons = persons.stream()
											  .filter(person -> person.getName() != null)
											  .filter(person -> person.getEmailAddress() != null)
											  .map(person -> person)
											  .collect(Collectors.toList());
		verifiedPersons.stream()
					   .forEach(person -> System.out.println(person.getName()));
	}

}
