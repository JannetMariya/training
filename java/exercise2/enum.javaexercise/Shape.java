/*+ compare the enum values using equal method and == operator

Requirment:
  To compare the enum values using equal method and == operator

Entity:
  Shape
  
Comprarision:
 1.If you compare any Enum with null, using == operator, it will result in false, but if you use equals() method to do this check, you may get NullPointerException
 2.== to compare enum is, compile time safety. Equality or == operator checks if both enum object are from same enum type or not at compile time itself, while equals() method will also return false but at runtime.
 3.Operator method is faster than calling method ,i.e == is fater than equals() method.*/

private enum Shape{ 
RECTANGLE, SQUARE, CIRCLE, TRIANGLE;
 } 
 private enum Status { 
 ON, OFF; 
 } 
 Shape unknown = null; 
 Shape circle = Shape.CIRCLE; 
 boolean result = unknown == circle; //return false 
 result = unknown.equals(circle); //throws NullPointerException

