/*
+ Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
	
Requirement:
	*To Demonstrate abstract classes using Shape class.	
	*To create methods for class to calculate area and perimeter
	*To define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
	
Entity
    1.Shape -Class name
    2.Circle - Class name 
    3.Rectangle - Class name
	
Jobs to be done Done
    1.First create Abstract Shape class with area Calculaing method as areaCalc
      and Perimeter Calculaing method as periCalc for both circle and rectangle
    2.Then Create Circle class and rectangle class to implement  area and perimeter calculating methods
    3.Creating Instance for the classes , calling the area,perimeter and area methods.
    4.Then pass parameter to calculate.
*/
	
abstract class Shape {
    abstract double areaCalc(int radius);
    abstract double periCalc(int radius);
    abstract int areaCalc(int width , int height);
    abstract int periCalc(int width , int height);
}

class Circle extends Shape {
    public double areaCalc(int radius) {
        return 3.14*radius*radius;
    }
    
    public double periCalc(int radius ) {
        return 2*3.14*radius;
    }
    int periCalc(int width , int height) {
        return 0;
    }
    int areaCalc(int width , int height) {
        return 0;
    }
}

class Rectangle extends Shape {
    int width;
    int height;
    public int areaCalc(int width,int height) {
        return width*height;
    }
    
    public int periCalc(int width,int height) {
        int x = width+height;
        return 2*x;
    }
    double areaCalc(int radius) {
        return 0;
    }
    double periCalc(int radius) {
        return 0;
    }
}

public class AbstractClass {
    public static void main(String[] args) {
        Shape circle = new Circle();
        int radius = 5;
        System.out.println("Area of Circle :"+circle.areaCalc(radius));
        System.out.println("Perimeter of Circle :"+circle.periCalc(radius));
        
        Rectangle rect = new Rectangle();
        int width = 2;
        int height = 3;
        System.out.println("Area of Rectangle :"+rect.areaCalc(width,height));
        System.out.println("Perimeter of Rectangle :"+rect.periCalc(width,height));
        
    }
}