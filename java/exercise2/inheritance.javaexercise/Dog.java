/*+ demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
    Problem Statement
    1.Demonstrate Inheritance, Overloading, Overriding 
      with Animal,Dog,Cat and Snake Class Objects
    
    Entity
    1.Animal Class (Base)
    2.Dog Class
    3.Cat Class
    4.Snake Class
    
    Work Done
    1.Initializing values in Animal Base Class
    2.Inheriting the Value in Cat and Snake Class and overloaded the methods.
    3.Created Object for Snake Class and Overridded the Methods
      and displayed
*/

class Animal {
    public int legs = 4;
    public int tail = 1;
    public void run() {
        System.out.println("Running");
    }
    
    //Method Overloading//
    
    public void run(String name) {
        System.out.println(name +" "+"Running");
    }
}

//Inherited Class//

class Cat extends Animal {
    public void animInfo() {
        System.out.println("Legs"+" "+legs);
        System.out.println("Tail"+" "+tail);
    }
}

class Snake extends Cat {
    
    //Method Overridding//
    
    public void run() {
        System.out.println("Moving");
    }
}

public class Dog {
    public static void main(String[] args) {
        Snake snake = new Snake();
        snake.legs = 3;
        System.out.println(snake.legs);
        
        //Object Calling//
        
        snake.animInfo();
        
        //Method Overridding//
        
        snake.run("Dog");
        
    }
}