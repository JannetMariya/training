+ Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?
	
Requirement:
  To find out the class and instance variable from the given code.
  
Entities:
  class name - IdentifyMyParts
  
Function Declaration:
  No function is declared in this program.
  
Job to be done:
  1. Read and Analyse the given code .
  2. Find the class variable and instance variable
  
Solution:
- What are the class variables?
	x is the class variable.
	Variable that is declared with static modifier is called class variable. 

- What are the instance variables?
	y is the instance variable.
	An instance variable is a variable defined inside a class. Here, y is the instance variable.
	
	
	
	
	
	
 
+ What is the output from the following code:
   IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
	
Requirement:
  To find the output of the given code

Entities:
   class name - IdentifyMyParts.

Function Declaration:
  No function is declared here.
  
Job to be done:
  1. Read and Analyse the given code .
  2. Find the output of the code
  
Solution:
output:
a.y= 5
b.y= 6
a.x= 2
b.x= 2
IdentifyMyParts.x= 2

Here, x is a static variable.It takes  2 beacuse the value last assigned to a static variable will be shared across all instance of the class. So,there is only one value for x.


