/*+ program to store and retrieve four boolean flags using a single int
Integer.valueOf(1).equals(Long.valueOf(1))
Requirement:
    1.Invert the value of a boolean and which operator would you use?
    
Entity
    1.BooleanInverter
    
 Jobs TO be Done
    1.Inverting the Boolean using the ! Symbol.
    2.Converting the true value of the Boolean to false using the ! Symbol..
*/
/*
    Answers
    1.We use ! Symbol to Invert the Boolean.
*/

public class BooleanInverter {
    public static void main(String[] args) {
        //To Invert the Value of the Boolean//
        boolean flag = false;
        String res = "Fail";
        if(res == "Pass") {
            System.out.println(flag);
        } else {
            flag = !flag;
            System.out.println(flag);
        }
    }
}