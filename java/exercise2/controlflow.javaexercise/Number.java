/*+ Consider the following code snippet.

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        - What output do you think the code will produce if aNumber is 3?
		
		
Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    It doesn't have any class name

Function Declaration:
    There is no function is declared in this program.

Jobs To Be Done:
    1)Considering the given program
    2)Checking the aNumber value greater than or equal to zero.
    3)It is true, So it moves to the next statement.
    4)Checking the aNumber value equal to zero 
    5)It is false, So it skips the step and move to the else part.
    6)There is no else part in these if type. So it moves to the else of previous if part.
    7)It prints the second string and followed by third string.
*/
OUTPUT:
second string
third string

+ Consider the following code snippet.

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        
-Write a test program containing the previous code snippet; make aNumber 3. What is the output of the program? Is it what you predicted? 
Explain why the output is what it is. In other words, what is the control flow for the code snippet?
  
  Solution: NestedIf


	second string
 third string
 3 is greater than or equal to 0, so execution progresses to the second if statement. The second if statement's test fails because 3 is not equal to 0. 
 Thus, the else clause executes (since it's attached to the second if statement). Thus, second string is displayed. 
 The final println is completely outside of any if statement, so it always gets executed, and thus third string is always displayed.	

- Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
		
		if (aNumber >= 0)
    if (aNumber == 0)
        System.out.println("first string");
    else
        System.out.println("second string");

System.out.println("third string");
		
		
		
- Use braces, { and }, to further clarify the code.


if (aNumber >= 0) {
    if (aNumber == 0) {
        System.out.println("first string");
    } else {
        System.out.println("second string");
    }
}

System.out.println("third string")		
		
		
		
