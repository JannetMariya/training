+ Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.

Requirement:
   To Consider the following string and answer the following questions
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.

Entities:
   No class is used in this program

Function Declaration:
   No function is declared in this program.

Solution:
   a)What is the value displayed by the expression hannah.length()?
   ans: The value is 32.
   b)What is the value returned by the method call hannah.charAt(12)?
   ans: e
   c)Write an expression that refers to the letter b in the string referred to by hannah.
   ans: hannah.charAt(15)