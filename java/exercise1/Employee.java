public class Employee{ // Class Signature 
  //Access Specifiers 
    public String employeeId;  //Property Declaration
    private String employeeName;
    protected String dateOfBirth;
    public Employee(String empId,String empName,String empdob) { //Parameterized Constructor 
        employeeId = empId;
        employeeName = empName;
        dateOfBirth = empdob;
    }
    public void printInfo() {  // Methods Declaration
        System.out.println("EmployeeName: " + employeeName);
        EmployeeDept empdept = new EmployeeDept("Physics");
        empdept.display(); //  To display Private Properties Using Methods 
    }
    class EmployeeDept{  // Inner Class 
        private String employeeDept;
        
        public EmployeeDept(String empDept) {
            employeeDept = empDept;
        }
        public void display() {
            System.out.println("EmpDept: " + employeeDept);
        }
    }
}
class Main{
    public static void main(String args[]) {
        Employee emp = new Employee("1001","Jannet","01-03-2001 ");
        emp.printInfo(); // To display Private Properties Using Methods
        System.out.println("EmployeeId: " + emp.employeeId); //Prints EmpID
        System.out.println("Employeedob: " + emp.dateOfBirth);
    }
}