#12.Create VIEW for your Join Query and select values from VIEW

use info;

create view news as
select teacher_name
from teacher
inner join student
on teacher.teacher_id = student.teacher_id;