# 2.Alter Table with Add new Column and Modify\Rename\Drop column
use info;
create table employee_info(employee_id numeric
                      , employee_name varchar(10)
                      , salary float
                      , department_id numeric); 
alter table employee_info 
add phone_no bigint ;

alter table employee_info 
modify  phone_no int(10)

alter table employee_info
rename column phone_no to mobile_no

alter table employee_info 
drop column  mobile_no; 


