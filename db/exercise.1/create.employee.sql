#1.Create Database\table and Rename\Drop Table

create database info;
use info;
create table employee_info(employee_id numeric
                      , employee_name varchar(10)
                      , salary float
                      , department_id numeric);
alter table employee_info rename to workers;
 drop table workers;