#9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

use info;
select employee_name
from employee_info
where department_id like '%102';

select employee_id
from employee_info
where employee_name='ajay' and salary=85000;

select employee_name
from employee_info
where salary=89000;

select employee_name
from employee_info
where not employee_id=3;

select employee_name
 from employee_info
 where employee_id in(2);