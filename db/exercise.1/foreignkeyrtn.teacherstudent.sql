#12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

use info;

select teacher_name
from teacher
inner join student
on teacher.teacher_id = student.teacher_id;

select teacher_name
from teacher
cross join student
on teacher.teacher_id = student.teacher_id;

select teacher_name
from teacher
left join student
on teacher.teacher_id = student.teacher_id;

select teacher_name
from teacher
right join student
on teacher.teacher_id = student.teacher_id;