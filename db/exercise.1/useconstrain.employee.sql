#3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX

use info;
create table teacher(teacher_name varchar(20)
                      ,teacher_id numeric(1)
                      ,subject_name varchar(5)
                      ,primary key(teacher_id));
                      
create table student(student_name varchar(10) not null
                     ,school_name varchar(30) default 'ABC'
                     ,rollno numeric(2) check (rollno>0)
                     ,email varchar(25)
                     ,mobileno numeric(10)
                     ,teacher_id numeric(1)
                     ,foreign key(teacher_id) references teacher(teacher_id)
                     ,primary key(rollno));
alter table teacher
add city varchar(10);

alter table student
add city varchar(10);
 