USE `education_database`;
SELECT student.`roll_number`
	  ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name`
      ,department.`dept_name`
      ,employee.`name`

  FROM department,student,employee,college_department,college,university
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id 
   AND employee.college_id = college.id
   AND employee.desig_id = 3
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'Madras University'
   AND college.city = 'Coimbatore'
   LIMIT 0,20;