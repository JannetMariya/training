USE `education_database`;
SELECT employee.id
	  ,employee.name
      ,employee.dob
      ,employee.desig_id
      ,designation.name
      ,designation.rank
      ,college.name
      ,university.univ_code
      ,university.university_name
  FROM employee,college,university,designation
 WHERE college.id = employee.college_id
       AND college.univ_code = university.univ_code
       AND designation.id = employee.desig_id
	   AND university.university_name = 'Anna University'
       ORDER BY college.name,designation.rank