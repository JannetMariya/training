select de.name as designation_name 
  ,rnk 
  ,u.univ_code
  ,c.college_NAME as college_name
  ,d.depT_name
  ,u.UNIVERSITY_NAME
  ,c.city,c.state
  ,c.year_opened
 from university u
  ,college c
  ,department d 
  ,designation de
  ,college_department cd
  ,employee e 
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.id 
 and cd.udept_code = d.dept_code
 and e.college_id = c.id 
 and e.cdept_id = cd.cdept_id 
 and e.desig_id = de.id 
 order by rnk;