SELECT student.roll_number
       ,student.name
       ,college.name
       ,semester_result.grade
       ,semester_result.credits
       ,semester_result.semester
       ,semester_result.gpa
FROM student
     ,semester_result
     ,college
WHERE student.id=semester_result.stud_id
   AND college.id=student.college_id
ORDER BY college.name,semester_result.semester
LIMIT 20
OFFSET 0;