select s.id as roll_number
  ,s.name as name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name
  ,d.DEPT_NAME
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.id 
 and cd.udept_code = d.dept_code
 and s.college_id = c.id
 and s.cdept_id = cd.cdept_id
 and s.academic_year = '2017';
 
 select u.UNIVERSITY_NAME
  ,s.id as roll_number
  ,s.name as name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name
  ,d.dept_name
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.id 
 and cd.udept_code = d.dept_code
 and s.college_id = c.id
 and s.cdept_id = cd.cdept_id
 and s.academic_year = '2017'
 and u.univ_code = '1001';
 
 select u.UNIVERSITY_NAME
  ,s.id as roll_number
  ,s.name as name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name
  ,d.dept_name
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.id 
 and cd.udept_code = d.dept_code
 and s.college_id = c.id
 and s.cdept_id = cd.cdept_id
 and s.academic_year = '2018'
 and u.univ_code = '1001'
 and c.city ='coimbatore';