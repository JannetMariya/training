CREATE TABLE `education_database`.`university` (`UNIV_CODE` CHAR(4) NOT NULL
                                              ,`UNIVERSITY_NAME` VARCHAR(100) NOT NULL,
                                               PRIMARY KEY (`UNIV_CODE`));

CREATE TABLE `education_database`.`college` (`ID` INT NOT NULL
                                           ,`CODE` CHAR(4) NOT NULL
                                           ,`NAME` VARCHAR(100) NOT NULL
                                           ,`UNIV_CODE` CHAR(4) NOT NULL
                                           ,`CITY` VARCHAR(50) NOT NULL
                                           ,`STATE` VARCHAR(50) NOT NULL
                                           ,`YEAR_OPENED` YEAR(4) NOT NULL
                                           ,PRIMARY KEY (`ID`)
                                           ,FOREIGN KEY (UNIV_CODE) REFERENCES university(UNIV_CODE));

CREATE TABLE `education_database`.`designation` (`ID` INT NOT NULL
                                               ,`NAME` VARCHAR(30) NOT NULL
                                               ,`RANK` CHAR(1) NOT NULL
                                               ,PRIMARY KEY (`ID`));
                                               
CREATE TABLE `education_database`.`department` (`DEPT_CODE` CHAR(4) NOT NULL
                                              ,`DEPT_NAME` VARCHAR(50) NOT NULL
                                              ,`UNIV_CODE` CHAR(4) NOT NULL
                                              ,PRIMARY KEY (`DEPT_CODE`)
                                              ,FOREIGN KEY (UNIV_CODE) 
                                               REFERENCES university(UNIV_CODE));
                                               
CREATE TABLE `education_database`.`college_department` (
  `CDEPT_ID` INT NOT NULL,
  `UDEPT_CODE` CHAR(4) NOT NULL,
  `COLLEGE_ID` INT NOT NULL,
    PRIMARY KEY (`CDEPT_ID`),
    FOREIGN KEY (UDEPT_CODE) REFERENCES department(DEPT_CODE),
    FOREIGN KEY (COLLEGE_ID) REFERENCES college(ID));
    
CREATE TABLE `education_database`.`employee` (
  `ID` INT NOT NULL,
  `NAME` VARCHAR(100) NOT NULL,
  `DOB` DATE NOT NULL,
  `EMAIL` VARCHAR(50) NOT NULL,
  `PHONE` BIGINT NOT NULL,
  `COLLEGE_ID` INT NOT NULL,
  `CDEPT_ID` INT NOT NULL,
  `DESIG_ID` INT NOT NULL,
    PRIMARY KEY (`ID`),
    FOREIGN KEY (COLLEGE_ID) REFERENCES designation(ID),
	FOREIGN KEY (CDEPT_ID) REFERENCES college_department(CDEPT_ID),
	FOREIGN KEY (DESIG_ID) REFERENCES designation(ID));

CREATE TABLE `education_database`.`syllabus` (
  `ID` INT NOT NULL,
  `CDEPT_ID` INT NOT NULL,
  `SYLLABUS_CODE` CHAR(4) NOT NULL,
  `SYLLABUS_NAME` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`ID`),
    FOREIGN KEY (CDEPT_ID) REFERENCES college_department(CDEPT_ID));

CREATE TABLE `education_database`.`profession_syllabus` (
  `EMP_ID` INT NOT NULL,
  `SYLLABUS_ID` INT NOT NULL,
  `SEMESTER` TINYINT NOT NULL,
    FOREIGN KEY (EMP_ID) REFERENCES employee(ID),
	FOREIGN KEY (SYLLABUS_ID) REFERENCES syllabus(ID));

CREATE TABLE `education_database`.`student` (
  `ID` INT NOT NULL,
  `ROLL_NUMBER` CHAR(8) NOT NULL,
  `NAME` VARCHAR(100) NOT NULL,
  `DOB` DATE NOT NULL,
  `GENDER` CHAR(1) NOT NULL,
  `EMAIL` VARCHAR(50) NOT NULL,
  `PHONE` BIGINT NOT NULL,
  `ADDRESS` VARCHAR(200) NOT NULL,
  `ACADEMIC_YEAR` YEAR(4) NOT NULL,
  `CDEPT_ID` INT NOT NULL,
  `COLLEGE_ID` INT NOT NULL,
  PRIMARY KEY (`ID`),
  FOREIGN KEY (CDEPT_ID) REFERENCES college_department(CDEPT_ID),
  FOREIGN KEY (COLLEGE_ID) REFERENCES college(ID));

CREATE TABLE `education_database`.`semester_result` (
  `STUD_ID` INT NOT NULL,
  `SYLLABUS_ID` INT NOT NULL,
  `SEMESTER` TINYINT NOT NULL,
  `GRADE` VARCHAR(2) NOT NULL,
  `CREDITS` FLOAT NOT NULL,
  `RESULT_DATE` DATE NOT NULL,
    FOREIGN KEY (STUD_ID) REFERENCES student(ID),
    FOREIGN KEY (SYLLABUS_ID) REFERENCES syllabus(ID));

CREATE TABLE `education_database`.`semester_fee` (
  `CDEPT_ID` INT NOT NULL,
  `STUD_ID` INT NOT NULL,
  `SEMESTER` TINYINT NOT NULL,
  `AMOUNT` DOUBLE NOT NULL,
  `PAID_YEAR` YEAR(4) NOT NULL,
  `PAID_STATUS` VARCHAR(10) NOT NULL,
    FOREIGN KEY (CDEPT_ID) REFERENCES college_department(CDEPT_ID),
    FOREIGN KEY (STUD_ID) REFERENCES student(ID));

