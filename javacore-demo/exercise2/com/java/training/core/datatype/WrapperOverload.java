package com.java.training.core.datatype;
/*+ demonstrate overloading with Wrapper types

Requirement:
    1.Overloading with Wrapper types
    
    Entity
    1.WrapperDemo Class
    2.WrapperOverLoad Class
    
Jobs to be  Done
    1.Created a WrapperDemo class to Demonstrate the Overloading with Wrapper Class.
    2.Created a method which gets the Primitive type int as the parameter and prints it.
    3.Created a method which gets the Primitive type char as the parameter and prints it.
    4.Created a method which gets the Primitiev type double as the parameter and prints it.
    5 In Main mehtod calling the wrapIntSetter method and passing the Wrapper type Integer as the argument.
    6.Calling the wrapCharSetter method and passing the Wrapper type Character as the argument.
    7.Caliing the warpDoubleSetter method and passing the Wrapper type Double as the argument.
*/

class WrapperDemo {
    void wrapIntSetter(int a) {
        System.out.println("int Value changed to Integer "+a);
    }
    
    void wrapcharSetter(char b) {
        System.out.println("char Value changed to Character "+b);
    }
    
    void wrapDoubleSetter(double c) {
        System.out.println("double Value changed to Double "+c);
    }
}

public class WrapperOverload {
    public static void main(String[] args) {
        //Overloading Wrapper types//
        
        Integer sd = 65;
        Character chara = 'S';
        Double fin = 3.14;
        WrapperDemo wrap = new WrapperDemo();
        wrap.wrapIntSetter(sd);
        wrap.wrapcharSetter(chara);
        wrap.wrapDoubleSetter(fin);
    }
}