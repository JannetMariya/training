package com.java.training.core.inheritance;
/*+ demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
    Problem Statement
    1.Demonstrate Inheritance, Overloading, Overriding 
      with Animal,Dog,Cat and Snake Class Objects
    
    Entity
    1.Animal  
    2.Dog 
    3.Cat 
    4.Snake 
    
    Work Done
    1.Declare and Initialize value in Animal Base Class
    2.Inherit the Value in Cat and Snake Class and overload the methods.
    3.Created Object for Snake Class and Override the Methods
      and display it
*/

class Animal {
    public int legs = 4;
    public int tail = 1;
    public void run() {
        System.out.println("Running");
    }
    
    //Method Overloading//
    
    public void run(String name) {
        System.out.println(name +" "+"Running");
    }
}

//Inherited Class//

class Cat extends Animal {
    public void animInfo() {
        System.out.println("Legs"+" "+legs);
        System.out.println("Tail"+" "+tail);
    }
}

class Snake extends Cat {
    
    //Method Overridding//
    
    public void run() {
        System.out.println("Moving");
    }
}

public class Dog {
    public static void main(String[] args) {
        Snake snake = new Snake();
        snake.legs = 1;
        System.out.println(snake.legs);
        
        //Object Calling//
        
        snake.animInfo();
        
        //Method Overridding//
        
        snake.run("Dog");
        
    }
}