/*+ print the absolute path of the .class file of the current class
+ open the source file of the running class in Notepad++ - do NOT hardcode/specify the source file in the code; find the source file using APIs
+ demonstrate object equality using Object.equals() vs ==, using String objects



    Requirement
    1.To print the absolute path of the .class file of the current class
    2.Open the source file of the running class in Notepad++ 
    3.To demonstrate object equality using 
      Object.equals() vs ==, using String objects
      
    Entity
    1.FilePath 
    
   Jobs to be done:
    1.Delcare a File object as file and specify the File name as Parameter.
    2.Then get the Absolute Path of a File using getAbsolutePath Function and print it.
    
    1.Get the Current Working File without HardCoding instead find it using APIs.
    2.By using getProperty method and passing the parameter as "user.dir" to get the directory of the working File.
    
    1.Comparing the  Object equality using equals() Function and == Operator.
    2.Declare a String Object as fHalf and initialize as "jannet"
    3.Declare another String Object as sHalf and initialize as "mariya"
    4.Declare anothre String object as tHalf and initialize as "jannet"
    5.Compare the String Objects fHalf and sHalf with equals method and print True if its True.
    6.Comapare the String Objects fHalf annd tHalf with == operator and print Flase if its True.
    
*/
package com.java.training.core.operator;
import java.io.*;

public class FilePath {
    public static void main(String[] args) {
        //Getting the Absolute Path of a File//
        File file = new File("FileDemo.class");
        System.out.println(file.getAbsolutePath());
        
        //Getting the Path of a File without HardCoding//
        System.out.println(System.getProperty("user.dir"));
        
        //String Object Comparision//
        String fHalf = "jannet";
        String sHalf = "mariya";
        String tHalf = "jannet";
        
        if(fHalf.equals(sHalf)) {
            System.out.println("True");
        } else if(fHalf == tHalf) {
            System.out.println("False");
        }
    }
}