package com.java.training.core.classobject;
/*+ Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?
	
Requirement:
  To find out the class and instance variable from the given code.
  
Entities:
  class name - IdentifyMyParts
  
Function Declaration:
  No function is declared in this program.
  
Job to be done:
  1. Read and Analyse the given code .
  2. Find the class variable and instance variable
  
Solution:
- What are the class variables?
	x is the class variable.
	Variable that is declared with static modifier is called class variable. 

- What are the instance variables?
	y is the instance variable.
	An instance variable is a variable defined inside a class. Here, y is the instance variable.
	
	
	
	
	
	
 
+ What is the output from the following code:*/

  public class IdentifyMyParts {
    public static int x = 7;
    public int y = 3;
    public static void main(String [] args) {
        
        //Class Variables are declared with static Signature//
        System.out.println("Class variables are x:"+x);
        
        IdentifyMyParts identify = new IdentifyMyParts();
        /*Instance Variables are declared with the instance of the
        particular Class and Called*/
        System.out.println("Instance Variable is y :"+identify.y);
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y); //Output: a.y = 5//
        System.out.println("b.y = " + b.y); //Output: b.y = 6//
        System.out.println("a.x = " + a.x); //Output: a.x = 2//
        System.out.println("b.x = " + b.x); //Output: b.x = 2//
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x); //IdentifyMyParts.x = 1//
    }
}
	
/*Requirement:
  To find the output of the given code

Entities:
   class name - IdentifyMyParts.

Function Declaration:
  No function is declared here.
  
Job to be done:
  1. Read and Analyse the given code .
  2. Find the output of the code
  
Solution:
output:
a.y= 5
b.y= 6
a.x= 2
b.x= 2
IdentifyMyParts.x= 2

Here, x is a static variable.It takes  2 beacuse the value last assigned to a static variable will be shared across all instance of the class. So,there is only one value for x.


*/