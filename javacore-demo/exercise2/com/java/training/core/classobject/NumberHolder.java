package com.java.training.core.classobject;
/*+ Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Requirments:
	Write Code to create Instant of a Class and initialize the members values and display the values for the given class.
	
Entities:
	NumberHolder
	
Function Declaration:None

Jobs To be done:
	Creating an Object for a Class and initializing the values  and d isplaying the member variables.

SOLTION: */

public class NumberHolder {
        public int anInt;
        public float aFloat;
        
        public static void main(String[] args) {
            NumberHolder numberholder = new NumberHolder();
            numberholder.anInt = 5;
            numberholder.aFloat = 25;
            System.out.println(numberholder.anInt);//Output:5//
            System.out.println(numberholder.aFloat);//Output:25.0//
        }
    }